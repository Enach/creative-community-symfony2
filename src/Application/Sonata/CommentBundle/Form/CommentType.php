<?php

namespace Application\Sonata\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * This is a FOSCommentBundle overridden form type
 */
class CommentType extends AbstractType {
	/**
	 * Is comment model implementing signed interface?
	 *
	 * @var boolean
	 */
	protected $isSignedInterface = false;

	/**
	 * Configures a Comment form.
	 *
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('authorName', 'text', array('required' => false))
			->add('website', 'url', array('required' => false,
										  'attr'     => array(
											  'placeholder' => 'http://',
										  )))
			->add('email', 'email', array('required' => false));
	}

	/**
	 * {@inheritdoc}
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'add_author' => !$this->isSignedInterface
		));
	}

	/**
	 * Sets if comment model is implementing signed interface
	 *
	 * @param boolean $isSignedInterface
	 */
	public function setIsSignedInterface($isSignedInterface) {
		$this->isSignedInterface = $isSignedInterface;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return "blog_comment_comment";
	}

	/**
	 * @return string
	 */
	public function getParent() {
		return "fos_comment_comment";
	}
}