<?php

namespace Application\Sonata\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Model\SignedCommentInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Sonata\CommentBundle\Entity\BaseComment as BaseComment;

class Comment extends BaseComment implements SignedCommentInterface {
	/**
	 * @var integer $id
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
	 * @var UserInterface
	 */
	protected $author;

	/**
	 * Get id
	 *
	 * @return integer $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setAuthor(UserInterface $author) {
		$this->author = $author;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthorName() {
		return $this->authorName;
	}
}