<?php

namespace Application\Sonata\CommentBundle\Entity;

use Sonata\CommentBundle\Entity\BaseThread as BaseThread;

class Thread extends BaseThread {
	/**
	 * @var integer $id
	 */
	protected $id;

	/**
	 * Get id
	 *
	 * @return integer $id
	 */
	public function getId() {
		return $this->id;
	}

}
