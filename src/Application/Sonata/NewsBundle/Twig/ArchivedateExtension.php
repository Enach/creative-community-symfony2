<?php

namespace Application\Sonata\NewsBundle\Twig;

class ArchivedateExtension extends \Twig_Extension {
	public function getFilters() {
		return array(
			new \Twig_SimpleFilter('convertdate', array($this, 'convertdateFilter')),
		);
	}

	public function convertdateFilter($date) {
		if ($date == '01') {
			$convertdate = 'Janvier';
		} elseif ($date == '02') {
			$convertdate = 'Février';
		} elseif ($date == '03') {
			$convertdate = 'Mars';
		} elseif ($date == '04') {
			$convertdate = 'Avril';
		} elseif ($date == '05') {
			$convertdate = 'Mai';
		} elseif ($date == '06') {
			$convertdate = 'Juin';
		} elseif ($date == '07') {
			$convertdate = 'Juillet';
		} elseif ($date == '08') {
			$convertdate = 'Août';
		} elseif ($date == '09') {
			$convertdate = 'Septembre';
		} elseif ($date == '10') {
			$convertdate = 'Octobre';
		} elseif ($date == '11') {
			$convertdate = 'Novembre';
		} elseif ($date == '12') {
			$convertdate = 'Décembre';
		}

		return $convertdate;
	}

	public function getName() {
		return 'archivedate_extension';
	}
}