<?php

namespace Application\Sonata\NewsBundle\Twig;

class Md5ConverterTwigExtension extends \Twig_Extension {

	public function getFilters() {
		return array(
			'md5' => new \Twig_Filter_Method($this, 'md5'),
		);
	}

	public function md5($string, $returnAsArray = false) {
		return md5($string);
	}

	public function getName() {
		return 'MD5 Converter';
	}
}