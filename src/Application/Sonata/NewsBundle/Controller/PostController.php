<?php

namespace Application\Sonata\NewsBundle\Controller;

use Application\Sonata\NewsBundle\Entity\Post;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\NewsBundle\Controller\PostController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Application\Sonata\NewsBundle\Entity\PostRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PostController extends BaseController {

	public function indexAction(array $criteria = array(), array $parameters = array()) {
		$pager = $this->getPostManager()->getPager(
			$criteria,
			$this->getRequest()->get('page', 1)
		);

		$parameters = array_merge(array(
			'pager'            => $pager,
			'blog'             => $this->get('sonata.news.blog'),
			'tag'              => false,
			'collection'       => false,
			'route'            => $this->getRequest()->get('_route'),
			'route_parameters' => $this->getRequest()->get('_route_params')
		), $parameters);

		$response = $this->render(sprintf('SonataNewsBundle:Post:index.%s.twig', $this->getRequest()->getRequestFormat()), $parameters);

		if ('rss' === $this->getRequest()->getRequestFormat()) {
			$response->headers->set('Content-Type', 'application/rss+xml');
		}

		return $response;
	}

	public function tagAction($tag, Request $request = null) {
		$tag = $this->get('sonata.classification.manager.tag')->findOneBy(array(
			'slug'    => $tag,
			'enabled' => true,
		));

		if (!$tag || !$tag->getEnabled()) {
			throw new NotFoundHttpException('Unable to find the tag');
		}

		return $this->renderArchiveTag(array('tag' => $tag->getSlug()), array('tag' => $tag));
	}

	public function renderArchiveTag(array $criteria = array(), array $parameters = array()) {
		$pager = $this->getPostManager()->getPager(
			$criteria,
			$this->getRequest()->get('page', 1)
		);

		$parameters = array_merge(array(
			'pager'            => $pager,
			'blog'             => $this->get('sonata.news.blog'),
			'tag'              => false,
			'collection'       => false,
			'route'            => $this->getRequest()->get('_route'),
			'route_parameters' => $this->getRequest()->get('_route_params'),
		), $parameters);

		$response = $this->render(sprintf('SonataNewsBundle:Post:archive_tag.%s.twig', $this->getRequest()->getRequestFormat()), $parameters);

		if ('rss' === $this->getRequest()->getRequestFormat()) {
			$response->headers->set('Content-Type', 'application/rss+xml');
		}

		return $response;
	}

	public function authorAction($author) {
		$author = $this->get('sonata.user.manager.user')->findOneBy(array(
			'username' => $author,
			'enabled'  => true,
		));

		if (!$author) {
			throw new NotFoundHttpException('Unable to find the author');
		}

		return $this->renderArchiveAuthor(array('author' => $author->getId()), array('author' => $author));
	}

	public function renderArchiveAuthor(array $criteria = array(), array $parameters = array()) {
		$pager = $this->getPostManager()->getPager(
			$criteria,
			$this->getRequest()->get('page', 1)
		);

		$parameters = array_merge(array(
			'pager'            => $pager,
			'blog'             => $this->get('sonata.news.blog'),
			'tag'              => false,
			'collection'       => false,
			'route'            => $this->getRequest()->get('_route'),
			'route_parameters' => $this->getRequest()->get('_route_params'),
		), $parameters);

		$response = $this->render(sprintf('SonataNewsBundle:Post:archive_author.%s.twig', $this->getRequest()->getRequestFormat()), $parameters);

		if ('rss' === $this->getRequest()->getRequestFormat()) {
			$response->headers->set('Content-Type', 'application/rss+xml');
		}

		return $response;
	}

	public function renderArchiveMonthly(array $criteria = array(), array $parameters = array()) {
		$pager = $this->getPostManager()->getPager(
			$criteria,
			$this->getRequest()->get('page', 1)
		);

		$parameters = array_merge(array(
			'pager'            => $pager,
			'blog'             => $this->get('sonata.news.blog'),
			'tag'              => false,
			'collection'       => false,
			'route'            => $this->getRequest()->get('_route'),
			'route_parameters' => $this->getRequest()->get('_route_params')
		), $parameters);

		$response = $this->render(sprintf('SonataNewsBundle:Post:archive_monthly.%s.twig', $this->getRequest()->getRequestFormat()), $parameters);

		if ('rss' === $this->getRequest()->getRequestFormat()) {
			$response->headers->set('Content-Type', 'application/rss+xml');
		}

		return $response;
	}

	public function archiveMonthlyAction($year, $month, Request $request = null) {
		$repository = $this->getDoctrine()->getManager()->getRepository('ApplicationSonataNewsBundle:Post');

		return $this->renderArchiveMonthly(array(
			'date' => $repository->getPublicationDateQueryParts(sprintf('%d-%d-%d', $year, $month, 1), 'month')
		));
	}

	public function archiveAction(Request $request = null) {
		$repository = $this->getDoctrine()->getManager()->getRepository('ApplicationSonataNewsBundle:Post');

		return $this->renderArchive(array(
			'dates' => $repository->getPublicationDateQueryParts(sprintf('%d-%d-%d', '', '', 1), 'month')
		));
	}

	public function viewAction($permalink, Request $request = null) {

		/** @var Post $post */
		$post       = $this->getPostManager()->findOneByPermalink($permalink, $this->container->get('sonata.news.blog'));
		$repository = $this->getDoctrine()->getManager()->getRepository('ApplicationSonataNewsBundle:Post');

		if ($post && $post->isPublic()) {

			$thread = $post->getCommentThread();

			if ($thread->getPermalink() != $permalink) {
				$thread->setPermalink($permalink);
			}

			if (null === $thread) {
				$thread = $this->container->get('fos_comment.manager.thread')->createThread();
				$thread->setPermalink($permalink);

				$this->container->get('fos_comment.manager.thread')->saveThread($thread);
			}

			$comments = $this->container->get('fos_comment.manager.comment')->findCommentTreeByThread($thread);

			$previous = $repository->getPreviousPost($post->getId());
			if (!empty($previous)) {
				$previous = $this->getBlog()->getPermalinkGenerator()->generate($previous, true);
			}

			$next = $repository->getNextPost($post->getId());
			if (!empty($next)) {
				$next = $this->getBlog()->getPermalinkGenerator()->generate($next, true);
			}
		}

		if (!$post || !$post->isPublic()) {
			throw new NotFoundHttpException('Aucun article');
		}

		if ($seoPage = $this->getSeoPage()) {
			$seoPage
				->setTitle($post->getTitle())
				->addMeta('name', 'description', $post->getAbstract())
				->addMeta('property', 'og:title', $post->getTitle())
				->addMeta('property', 'og:type', 'blog')
				->addMeta('property', 'og:url', $this->generateUrl('sonata_news_view', array(
					'permalink' => $this->getBlog()->getPermalinkGenerator()->generate($post, true),
				), true))
				->addMeta('property', 'og:description', $post->getAbstract());
		}

		return $this->render('SonataNewsBundle:Post:view.html.twig', array(
			'post'     => $post,
			'previous' => $previous,
			'next'     => $next,
			'comments' => $comments,
			'thread'   => $thread,
			'form'     => false,
			'blog'     => $this->get('sonata.news.blog'),
		));
	}

}