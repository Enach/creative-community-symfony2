<?php

namespace Application\Sonata\NewsBundle\Entity;

use Sonata\NewsBundle\Entity\BasePostRepository;
use Sonata\NewsBundle\Model\BlogInterface;

class PostRepository extends BasePostRepository {
	public function findAll() {
		$qb = $this->createQueryBuilder('p');

		$qb->orderBy('p.publicationDateStart', 'DESC');

		return $qb
			->getQuery()
			->getResult();
	}

	public function getPublicationDateQueryParts($date, $step, $alias = 'p') {
		return array(
			'query'  => sprintf('%s.publicationDateStart >= :startDate AND %s.publicationDateStart < :endDate', $alias, $alias),
			'params' => array(
				'startDate' => new \DateTime($date),
				'endDate'   => new \DateTime($date . '+1 ' . $step),
			),
		);
	}

	/**
	 * @param string $collection
	 *
	 * @return array
	 */
	public function getPublicationCollectionQueryParts($collection) {
		$pcqp = array('query' => '', 'params' => array());

		if (null === $collection) {
			$pcqp['query'] = 'p.collection IS NULL';
		} else {
			$pcqp['query']  = 'c.slug = :collection';
			$pcqp['params'] = array('collection' => $collection);
		}

		return $pcqp;
	}

	public function getPreviousPost($id) {
		$qb = $this->createQueryBuilder('p');

		$qb->where('p.id < :id')
			->setParameter('id', $id)
			->orderBy('p.id', 'DESC')
			->setMaxResults(1);

		return $qb
			->getQuery()
			->getOneOrNullResult();
	}

	public function getNextPost($id) {
		$qb = $this->createQueryBuilder('p');

		$qb->where('p.id > :id')
			->setParameter('id', $id)
			->orderBy('p.id', 'ASC')
			->setMaxResults(1);

		return $qb
			->getQuery()
			->getOneOrNullResult();
	}
}