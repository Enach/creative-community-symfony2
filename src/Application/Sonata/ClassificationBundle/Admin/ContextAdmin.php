<?php

namespace Application\Sonata\ClassificationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ContextAdmin extends Admin {
	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('id')
			->add('name')
			->add('enabled', null, array('required' => false))
			->add('createdAt');
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
		$datagridMapper
			->add('id')
			->add('name')
			->add('enabled');
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->addIdentifier('name')
			->addIdentifier('id')
			->add('enabled', null, array('editable' => true))
			->add('createdAt')
			->add('updatedAt');
	}
}
