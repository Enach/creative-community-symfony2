<?php

namespace CreaCom\ProjectBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class VoiceActorAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales', array(
				'class' => 'col-md-6',
			))
			->add('pseudo', 'text', array(
				'label' => "Nom",
			))
			->add('user', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('dateOfStart', 'date', array(
				'label' => "Date de début",
			))
			->end()
			->with('Informations Personnelle', array(
				'class' => 'col-md-6',
			))
			->add('twitter', 'text', array(
				'required' => false,
				'label'    => "Twitter",
			))
			->add('facebook', 'text', array(
				'required' => false,
				'label'    => "Facebook",
			))
			->add('site', 'text', array(
				'required' => false,
				'label'    => "Site",
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('pseudo')
			->add('user.username')
			->add('dateOfStart')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}