<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProjectAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('name', 'text', array(
				'label' => "Nom",
			))
			->add('type', 'entity', array(
				'required' => false,
				'class'    => 'CreaComProjectBundle:TypeProject',
				'property' => 'name',
			))
			->add('description', 'textarea', array(
				'label' => "Description",
				'attr'  => array('class' => 'tinymce'),
			))
			->end()
			->with('Informations Générales (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('banniere', new ImageType(), array(
				'label' => "Image",
			))
			->add('responsable', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('public', 'checkbox', array(
				'label'    => 'Public ?',
				'required' => false,
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('name')
			->add('type.name')
			->add('public')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}