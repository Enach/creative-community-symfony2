<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class RolesAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales', array(
				'class' => 'col-md-12',
			))
			->add('name', 'text', array(
				'label' => "Nom",
			))
			->add('project', 'entity', array(
				'required' => false,
				'class'    => 'CreaComProjectBundle:Project',
				'property' => 'name',
			))
			->add('recurrant', 'checkbox', array(
				'label'    => 'Récurrent ?',
				'required' => false,
			))
			->add('revenant', 'checkbox', array(
				'label'    => 'Revenant ?',
				'required' => false,
			))
			->add('ponctuel', 'checkbox', array(
				'label'    => 'Ponctuel ?',
				'required' => false,
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('name')
			->add('project.name')
			->add('recurrant', 'text', array('label' => 'Récurrent'))
			->add('revenant')
			->add('ponctuel')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}