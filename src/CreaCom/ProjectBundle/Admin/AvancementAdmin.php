<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AvancementAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales', array(
				'class' => 'col-md-12',
			))
			->add('episode', 'entity', array(
				'class'    => 'CreaComProjectBundle:Episode',
				'property' => 'name',
			))
			->add('name', 'text', array(
				'label' => 'Nom à afficher',
			))
			->add('progress', 'number', array(
				'label' => 'Progrès sur 100',
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('episode.name')
			->add('name')
			->add('progress')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}