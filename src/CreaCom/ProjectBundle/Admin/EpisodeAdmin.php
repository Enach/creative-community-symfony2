<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EpisodeAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('numero', 'number', array(
				'label' => 'Numéro',
			))
			->add('name', 'text', array(
				'label' => 'Nom',
			))
			->add('deadline', 'datetime', array(
				'label'    => 'DeadLine',
				'required' => false,
			))
			->add('youtube', 'text', array(
				'label'    => 'Youtube',
				'required' => false,
			))
			->add('miniature', new ImageType(), array(
				'label'    => 'Miniature',
				'required' => false,
			))
			->add('public', 'checkbox', array(
				'label'    => 'Public ?',
				'required' => false,
			))
			->end()
			->with('Informations Générales (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('saison', 'entity', array(
				'class'    => 'CreaComProjectBundle:Saison',
				'property' => 'name',
			))
			->add('auteurs', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
				'expanded' => false,
				'multiple' => true,
			))
			->add('realisateurs', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
				'expanded' => false,
				'multiple' => true,
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('saison.name')
			->add('name')
			->add('deadline')
			->add('public')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}