<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CastingAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales', array(
				'class' => 'col-md-6',
			))
			->add('voiceActing', 'entity', array(
				'class'    => 'CreaComProjectBundle:VoiceActing',
				'property' => 'name',
			))
			->add('role', 'entity', array(
				'class'    => 'CreaComProjectBundle:Roles',
				'property' => 'name',
			))
			->add('voiceActor', 'entity', array(
				'class'    => 'CreaComProjectBundle:VoiceActor',
				'property' => 'pseudo',
			))
			->add('deadline', 'date', array(
				'required' => false,
				'label'    => 'DeadLine',
			))
			->end()
			->with('Avancement', array(
				'class' => 'col-md-6',
			))
			->add('etat', 'text', array(
				'required' => false,
				'label'    => 'Etat',
			))
			->add('nextStep', 'date', array(
				'required' => false,
				'label'    => 'Prochaine étape',
			))
			->add('comment', 'textarea', array(
				'required' => false,
				'label'    => 'Commentaire',
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('voiceActing.name')
			->add('role.name')
			->add('voiceActor.pseudo')
			->add('deadline')
			->add('etat')
			->add('nextStep')
			->add('comment')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}