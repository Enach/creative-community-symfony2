<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class VoiceActingAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales', array(
				'class' => 'col-md-12',
			))
			->add('episode', 'entity', array(
				'required' => false,
				'class'    => 'CreaComProjectBundle:Episode',
				'property' => 'numero',
			))
			->add('responsable', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('deadline', 'date', array(
				'label'    => 'Deadline',
				'required' => false,
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('episode.name')
			->add('responsable.username')
			->add('deadline')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}