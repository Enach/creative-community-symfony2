<?php

namespace CreaCom\ProjectBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TournageAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('episodes', 'entity', array(
				'required' => false,
				'class'    => 'CreaComProjectBundle:Episode',
				'property' => 'UniqueName',
				'expanded' => false,
				'multiple' => true,
			))
			->add('participants', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
				'expanded' => false,
				'multiple' => true,
			))
			->add('date', 'datetime', array(
				'label' => "Date",
			))
			->end()
			->with('Informations Générales (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('lieu', 'text', array(
				'label' => "Lieu (Serveur et Map)",
			))
			->add('skins', 'textarea', array(
				'label' => "Skins et role associé + (pseudo)",
			))
			->add('script', 'textarea', array(
				'label' => "Script",
			))
			->add('fait', 'checkbox', array(
				'label'    => 'Fait ?',
				'required' => false,
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('name')
			->add('date')
			->add('lieu')
			->add('fait')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));

	}
}