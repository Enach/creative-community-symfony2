<?php
namespace CreaCom\ProjectBundle\Form;

use CreaCom\ProjectBundle\Entity\HelpRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Nietonfir\Google\ReCaptchaBundle\Controller\ReCaptchaValidationInterface;

class HelpRequestType extends AbstractType implements ReCaptchaValidationInterface {

	public function buildForm(FormBuilderInterface $builder, array $options) {

		$settings = array(
			'width'        => 206,
			'height'       => 57,
			'font_size'    => 22,
			'length'       => 7,
			'border_color' => "cccccc"
		);
		/** @var HelpRequest $request */
		$request = $options['data'];
		$builder
			->add('nom', 'text', array('label' => ($request->getType() != "contact" ? "Nom et prénom du porteur de projet *" : "Nom et prénom *")))
			->add('email', 'text', array('label' => ($request->getType() != "contact" ? "Adresse mail du porteur de projet *" : "Adresse mail *")))
			->add('message', 'textarea', array('label' => ($request->getType() != "contact" ? "Présentation détaillée du projet *" : "Message *")));
		if ($request->getType() == "doublage") {
			$builder->add('nbVoix', 'text', array('label' => "Nombre de voix nécessaires (féminine et masculine) *"));
		} elseif ($request->getType() == "ateliers") {
			$builder
				->add('lieu', 'text', array('label' => 'Lieu d’organisation de l’atelier', 'required' => false))
				->add('dates', 'text', array('label' => 'Date(s)', 'required' => false))
				->add('atelier_type', 'entity', array('class' => 'CreaCom\ProjectBundle\Entity\Ateliers', 'property' => 'nom'));
		}
		if ($request->getType() != 'contact') {
			$builder->add('organisme', 'text', array('label' => 'Organisme ou structure', 'required' => false))
				->add('more', 'textarea', array('label' => 'Autres informations éventuelles', 'required' => false));
		}
		$builder
			->add('valider', 'submit', array('attr' => array('class' => 'btn btn-default'), 'label' => "Envoyer"))
			->add('recaptcha', 'recaptcha', array('mapped' => false));
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'CreaCom\ProjectBundle\Entity\HelpRequest',
		));
	}

	public function getName() {
		return 'creacom_sitecreacom_help_request';
	}

}