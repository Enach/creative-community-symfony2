<?php

namespace CreaCom\ProjectBundle\Services;

class IndexServices {
	public function getServices() {
		return array(
			array("RECRUTEMENT", "", "Envie de te faire de nouveaux amis tout en participant à des projets passionnants et stimulants ?\nToute l’équipe t’attends!", "web/bundles/creacomproject/index/recrutement.png"),
			array("DOUBLAGE", "", "Besoin de voix pour vos projets ? Quel que soit le medium, nos acteurs sont à votre disposition pour donner vie à vos personnages.", "web/bundles/creacomproject/index/doublage.png"),
			array("TRADUCTION", "français - anglais", "Grâce à nos traducteurs, vos rêves de grandeur à l'internationnal vont enfin pouvoir se réaliser!", "web/bundles/creacomproject/index/traduction.png"),
			array("COURT-MÉTRAGE", "", "De nombreux court-métrages à découvrir avec l'exploration d'univers vastes et variés: science-fiction, fantasy, fantastique, tout y passe !", "web/bundles/creacomproject/index/courtmetrages.png"),
			array("ATELIERS", "", "Création de machinima, ateliers pédagogiques, ou simple découverte du média, nous organisons toutes sortes d'activités autour du machinima.", "web/bundles/creacomproject/index/machinima.png"),
			array("MACHIMANIA", "", "Tout le machinima concentré en un seul site. Des actus, des critiques, et de nombreuses émissions pour découvrirce nouveau genre cinématographique.", "web/bundles/creacomproject/index/machimania.png")
		);
	}

}