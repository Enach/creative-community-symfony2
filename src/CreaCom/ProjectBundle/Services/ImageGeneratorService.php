<?php

namespace CreaCom\ProjectBundle\Services;

use CreaCom\ProjectBundle\Entity\Project;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ImageGeneratorService {
	use ContainerAwareTrait;

	public function createProjectBanner(Project $projet, $position = "droit", $index = false) {
		$text = ($position == "droit" ? $projet->getSortie() : $projet->getName() . ' : ' . $projet->getSortie());

		if (!$index) {
			$imageProjet = imagecreatefromjpeg($this->container->get('kernel')->locateResource('@CreaComProjectBundle/Resources/public/images/projets/' . $projet->getSlug() . '/banner.jpg'));
			list($width, $height) = getimagesize($this->container->get('kernel')->locateResource('@CreaComProjectBundle/Resources/public/images/projets/' . $projet->getSlug() . '/banner.jpg'));
		} else {
			$imageProjet = imagecreatefromjpeg($this->container->get('kernel')->locateResource('@CreaComProjectBundle/Resources/public/images/index/' . $projet->getSlug() . '.jpg'));
			list($width, $height) = getimagesize($this->container->get('kernel')->locateResource('@CreaComProjectBundle/Resources/public/images/index/' . $projet->getSlug() . '.jpg'));
		}
		$font        = $this->container->get('kernel')->locateResource('@CreaComProjectBundle/Resources/public/css/font/BebasNeue.otf');
		$white       = imagecolorallocate($imageProjet, 255, 255, 255);
		$type_space  = imagettfbbox(($height / 22), 0, $font, $text);
		$ascent      = abs($type_space[7]);
		$descent     = abs($type_space[1]);
		$image_width = abs($type_space[4] - $type_space[0]);
		$heightText  = $ascent + $descent;

		$position = ($position == "droit" ? $width - ($image_width + 25) : 25);

		imagettftext($imageProjet, ($height / 22), 0, $position, $height - ($heightText + 5), $white, $font, $text);
		imagefilter($imageProjet, IMG_FILTER_SMOOTH, 50);
		ob_start();
		imagepng($imageProjet);
		$imagevariable = ob_get_contents();
		imagedestroy($imageProjet);
		ob_end_clean();

		return base64_encode($imagevariable);
	}
}