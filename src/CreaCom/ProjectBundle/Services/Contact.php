<?php

namespace CreaCom\ProjectBundle\Services;

class Contact {

	public function getAll() {
		return array('ts3server://ts.creativecommunity.fr', 'skype:creative.community?chat', 'https://twitter.com/Creat_Community', 'https://www.facebook.com/CreativeCommunityStudio/');
	}

}