<?php

namespace CreaCom\ProjectBundle\Controller;

use CreaCom\ProjectBundle\Entity\HelpRequest;
use CreaCom\ProjectBundle\Entity\Project;
use CreaCom\ProjectBundle\Form\HelpRequestType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nietonfir\Google\ReCaptchaBundle\Controller\ReCaptchaValidationInterface;

class DefaultController extends Controller implements ReCaptchaValidationInterface {

	/**
	 * @Route("/", name="creacom_sitecreacom_index")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction() {

		$starcube       = $this->get('doctrine')->getRepository('CreaComProjectBundle:Project')->findOneBy(array('slug' => 'starcube'));
		$imgStarcube    = $this->get('creacom.image_generator_service')->createProjectBanner($starcube, "droit", true);
		$stasiapolis    = $this->get('doctrine')->getRepository('CreaComProjectBundle:Project')->findOneBy(array('slug' => 'stasiapolis'));
		$imgStasiapolis = $this->get('creacom.image_generator_service')->createProjectBanner($stasiapolis, "droit", true);
		$moebius        = $this->get('doctrine')->getRepository('CreaComProjectBundle:Project')->findOneBy(array('slug' => 'moebius'));
		$imgMoebius     = $this->get('creacom.image_generator_service')->createProjectBanner($moebius, 'gauche', true);

//		return $this->render('CreaComProjectBundle:SiteCreaCom:index.html.twig',
//			$this->get('sitecreacom.indexservices')->getServices(),
//			array('lesActualites' => $this->getDoctrine()->getRepository('CreaComProjectBundle:PageIndex')->getLatest()->getLesActualites()));

		return $this->render('CreaComProjectBundle:Default:accueil.html.twig', array('imgStarcube' => $imgStarcube, 'imgStasiapolis' => $imgStasiapolis, 'imgMoebius' => $imgMoebius, 'starcube' => $starcube, 'stasiapolis' => $stasiapolis));
	}

	/**
	 * @Route("/contact", name="creacom_sitecreacom_contact")
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function contactAction(Request $request) {

		$helpRequest = new HelpRequest();
		$helpRequest->setType('contact');

		$form = $this->createForm(new HelpRequestType(), $helpRequest);

		if ($form->handleRequest($request)->isValid()) {

			try {
				//persist entity
				$em = $this->getDoctrine()->getManager();
				$em->persist($helpRequest);
				$em->flush();

				//send mail
				\Swift_Message::newInstance()
					->setSubject('Nouveau message contact')
					->setFrom($helpRequest->getEmail())
					->setTo('contact@creativecommunity.fr')
					->setBody(
						$this->render('CreaComProjectBundle:SiteCreaCom:contrib_mail.html.twig', array('request' => $helpRequest)),
						'text/html'
					);

				\Swift_Message::newInstance()
					->setSubject('Creative Community - Votre demande de contact')
					->setFrom('contact@creativecommunity.fr')
					->setTo($helpRequest->getEmail())
					->setBody(
						$this->render('CreaComProjectBundle:SiteCreaCom:contrib_mail.html.twig', array('request' => $helpRequest, 'customer' => true)),
						'text/html'
					);

				$this->get('session')->getFlashBag()->clear();
				$this->get('session')->getFlashBag()->add('statut', 'Votre demande a été soumise. Pensez à vérifier votre boîte-mail.');
			} catch (\Exception $e) {
				$this->get('session')->getFlashBag()->clear();
				$this->get('session')->getFlashBag()->add('statut', 'Erreur - Votre demande n\'a pas pu aboutir.<br>' . $e->getMessage() . '<br>Si le problème persiste, merci de contacter un administrateur.');
			}

			return $this->redirectToRoute('creacom_sitecreacom_contact');
		}

		return $this->render('CreaComProjectBundle:SiteCreaCom:contact.html.twig', array('form' => $form->createView(), 'contacts' => $this->get('doctrine')->getRepository('CreaComProjectBundle:Contact')->findAll()));
	}

	/**
	 * @Route("/contrib/{type}", name="creacom_sitecreacom_contrib", defaults={"type"="doublage"})
	 *
	 * @param Request $request
	 * @param         $type
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function contribAction(Request $request, $type) {
		$helpRequest = new HelpRequest();
		$helpRequest->setType($type);

		$form = $this->createForm(new HelpRequestType(), $helpRequest);

		if ($form->handleRequest($request)->isValid()) {

			try {
				//persist entity
				$em = $this->getDoctrine()->getManager();
				$em->persist($helpRequest);
				$em->flush();

				//send mail
				$message = \Swift_Message::newInstance()
					->setSubject('Nouvelle demande de ' . $type)
					->setFrom($helpRequest->getEmail())
					->setTo('contact@creativecommunity.fr')
					->setBody(
						$this->render('CreaComProjectBundle:SiteCreaCom:contrib_mail.html.twig', array('request' => $helpRequest)),
						'text/html'
					);

				$message = \Swift_Message::newInstance()
					->setSubject('Creative Community - Votre demande de ' . $type)
					->setFrom('contact@creativecommunity.fr')
					->setTo($helpRequest->getEmail())
					->setBody(
						$this->render('CreaComProjectBundle:SiteCreaCom:contrib_mail.html.twig', array('request' => $helpRequest, 'customer' => true)),
						'text/html'
					);

				$this->get('session')->getFlashBag()->clear();
				$this->get('session')->getFlashBag()->add('statut', 'Votre demande a été soumise. Pensez à vérifier votre boîte-mail.');
			} catch (\Exception $e) {
				$this->get('session')->getFlashBag()->clear();
				$this->get('session')->getFlashBag()->add('statut', 'Erreur - Votre demande n\'a pas pu aboutir.<br>' . $e->getMessage() . '<br>Si le problème persiste, merci de contacter un administrateur.');
			}

			return $this->redirectToRoute('creacom_sitecreacom_contrib', array('type' => $type));
		}
		$lesContributions = $this->getDoctrine()->getRepository('CreaComProjectBundle:Contribution')->findBy(array('type' => $type));

		return $this->render('CreaComProjectBundle:SiteCreaCom:contrib.html.twig',
			array('form'             => $form->createView(),
				  'lesContributions' => $lesContributions,
				  'type'             => $type
			));
	}

	/**
	 * @Route("/partenaires", name="creacom_sitecreacom_partenaires")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function partenairesAction() {
		return $this->render('CreaComProjectBundle:SiteCreaCom:partenaires.html.twig',
			array('lesPartenaires' => $this->getDoctrine()->getRepository('CreaComProjectBundle:Partenaire')->findAll()));
	}

	/**
	 * @Route("/recrutement", name="creacom_sitecreacom_recrutement")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function recrutementAction() {
		return $this->render('CreaComProjectBundle:SiteCreaCom:recrutement.html.twig',
			array('lesPostesOpen'  => $this->getDoctrine()->getRepository('CreaComProjectBundle:Poste')->findBy(array('show' => true, 'statut' => true)),
				  'lesPostesClose' => $this->getDoctrine()->getRepository('CreaComProjectBundle:Poste')->findBy(array('show' => true, 'statut' => false))));
	}

	/**
	 * @Route("/presentation", name="creacom_sitecreacom_presentation")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function presentationAction() {

		return $this->render('CreaComProjectBundle:SiteCreaCom:presentation.html.twig',
			array('leStaff' => $this->getDoctrine()->getRepository('CreaComProjectBundle:Membre')->getMembresStaff(), 'lesRemerciements' => $this->getDoctrine()->getRepository('CreaComProjectBundle:Membre')->getRemerciments()));
	}

	/**
	 * @Route("/projet/{slug}", name="creacom_sitecreacom_projet")
	 * @ParamConverter("project", options={"mapping": {"slug": "slug"}})
	 *
	 * @param Project $project
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function projetAction(Project $project) {

		return $this->render('CreaComProjectBundle:SiteCreaCom:projet.html.twig', array('project' => $project));
	}

	/**
	 * @Route("/projets", name="creacom_sitecreacom_projets")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function projectsAction() {

		$projects = $this->get('doctrine')->getRepository('CreaComProjectBundle:Project')->findBy(array('public' => true));
		usort($projects, function (Project $a, Project $b) {
			if ($a->getOrder() == $b->getOrder()) {
				return 0;
			}

			return ($a->getOrder() < $b->getOrder()) ? -1 : 1;
		});
		$banners = array();
		foreach ($projects as $project) {
			$banners[$project->getId()] = $this->get('creacom.image_generator_service')->createProjectBanner($project);
		}

		return $this->render('CreaComProjectBundle:SiteCreaCom:projets.html.twig', array('projects' => $projects, 'banners' => $banners));
	}

//	/**
//	 * @param $idSerie
//	 * @param $idSaison
//	 *
//	 * @return \Symfony\Component\HttpFoundation\Response
//	 */
//	public function projetsAction($idSerie, $idSaison) {
//		$type       = array();
//		$extras     = null;
//		$checkSerie = $this->getDoctrine()->getRepository('CreaComProjectBundle:Project')->getProjectFromId($idSerie);
//		if ($idSerie == 0 && $idSaison == 0) {
//			$type   = array('type' => "projets");
//			$extras = $this->getDoctrine()->getRepository('CreaComProjectBundle:Project')->findAll();
//		} else if ($idSerie > 0 && $idSaison == 0) {
//			if (null !== $checkSerie) {
//				if (!$this->getDoctrine()->getRepository('CreaComProjectBundle:Project')->getProjectFromId($idSerie)->isSerie()) {
//					$type   = array('type' => "courtmetrage");
//					$extras = array();
//				} else {
//					$type   = array('type' => "saisons");
//					$extras = $this->getDoctrine()->getRepository('CreaComProjectBundle:Project')->getProjectFromId($idSerie)->getSaisons();
//				}
//			} else {
//				throw new Exception();
//			}
//		} else if ($idSerie > 0 && $idSaison > 0 && null !== $checkSerie && $this->getDoctrine()->getRepository('CreaComProjectBundle:Saison')->getSaisonFromId($this->getDoctrine()->getRepository('CreaComProjectBundle:Project')->getProjectFromId($idSerie), $idSaison) != null) {
//
//			$type   = array('type' => "episodes");
//			$extras = $this->getDoctrine()->getRepository('CreaComProjectBundle:Saison')->getSaisonFromId($this->getDoctrine()->getRepository('CreaComProjectBundle:Project')->getProjectFromId($idSerie), $idSaison)->getEpisodes();
//
//		} else {
//			throw new Exception();
//		}
//
//		return $this->render('CreaComProjectBundle:SiteCreaCom:projets.html.twig',array(
//		    $type,
//			$extras
//        ));
//	}

	/**
	 * @Route("/legales", name="creacom_sitecreacom_mentionslegales")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function mentionsLegalesAction() {
		return $this->render('CreaComProjectBundle:SiteCreaCom:mentionslegales.html.twig');
	}

}
