<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * Avancement
 *
 * @ORM\Table(name="episode_avancement")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\AvancementRepository")
 */
class Avancement {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Episode", fetch="EXTRA_LAZY", inversedBy="progression")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="avencement_episode",
	 *      joinColumns={@JoinColumn(name="avencement_episode", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id", unique=true)}
	 * )
	 */
	private $episode;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="progress", type="integer")
	 */
	private $progress = 0;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="datetime", type="datetime")
	 */
	private $datetime;

	/**
	 * Avancement constructor.
	 */
	public function __construct() {
		$this->datetime = new \DateTime();
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get episode
	 *
	 * @return string
	 */
	public function getEpisode() {
		return $this->episode;
	}

	/**
	 * Set episode
	 *
	 * @param string $episode
	 *
	 * @return Avancement
	 */
	public function setEpisode($episode) {
		$this->episode = $episode;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Avancement
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get progress
	 *
	 * @return integer
	 */
	public function getProgress() {
		return $this->progress;
	}

	/**
	 * Set progress
	 *
	 * @param integer $progress
	 *
	 * @return Avancement
	 */
	public function setProgress($progress) {
		$this->progress = $progress;

		return $this;
	}

	/**
	 * Get datetime
	 *
	 * @return \DateTime
	 */
	public function getDatetime() {
		return $this->datetime;
	}

	/**
	 * Set datetime
	 *
	 * @param \DateTime $datetime
	 *
	 * @return Avancement
	 */
	public function setDatetime($datetime) {
		$this->datetime = $datetime;

		return $this;
	}
}

