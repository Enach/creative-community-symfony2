<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Casting
 *
 * @ORM\Table(name="voice_acting_casting")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\CastingRepository")
 */
class Casting {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\VoiceActing", fetch="EXTRA_LAZY", inversedBy="voiceActing")
	 */
	private $voiceActing;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Roles", fetch="EXTRA_LAZY")
	 */
	private $role;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\VoiceActor", fetch="EXTRA_LAZY")
	 */
	private $voiceActor;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="etat", type="string", length=255)
	 */
	private $etat;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="deadline", type="string", length=255, nullable=true)
	 */
	private $deadline;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="nextStep", type="date", nullable=true)
	 */
	private $nextStep;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="comment", type="text", nullable=true)
	 */
	private $comment;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get voiceActing
	 *
	 * @return string
	 */
	public function getVoiceActing() {
		return $this->voiceActing;
	}

	/**
	 * Set voiceActing
	 *
	 * @param string $voiceActing
	 *
	 * @return Casting
	 */
	public function setVoiceActing($voiceActing) {
		$this->voiceActing = $voiceActing;

		return $this;
	}

	/**
	 * Get role
	 *
	 * @return Roles
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * Set role
	 *
	 * @param Roles $role
	 *
	 * @return Casting
	 */
	public function setRole(Roles $role) {
		$this->role = $role;

		return $this;
	}

	/**
	 * Get voiceActor
	 *
	 * @return string
	 */
	public function getVoiceActor() {
		return $this->voiceActor;
	}

	/**
	 * Set voiceActor
	 *
	 * @param string $voiceActor
	 *
	 * @return Casting
	 */
	public function setVoiceActor($voiceActor) {
		$this->voiceActor = $voiceActor;

		return $this;
	}

	/**
	 * Get etat
	 *
	 * @return string
	 */
	public function getEtat() {
		return $this->etat;
	}

	/**
	 * Set etat
	 *
	 * @param string $etat
	 *
	 * @return Casting
	 */
	public function setEtat($etat) {
		$this->etat = $etat;

		return $this;
	}

	/**
	 * Get deadline
	 *
	 * @return string
	 */
	public function getDeadline() {
		return $this->deadline;
	}

	/**
	 * Set deadline
	 *
	 * @param string $deadline
	 *
	 * @return Casting
	 */
	public function setDeadline($deadline) {
		$this->deadline = $deadline;

		return $this;
	}

	/**
	 * Get nextStep
	 *
	 * @return \DateTime
	 */
	public function getNextStep() {
		return $this->nextStep;
	}

	/**
	 * Set nextStep
	 *
	 * @param \DateTime $nextStep
	 *
	 * @return Casting
	 */
	public function setNextStep($nextStep) {
		$this->nextStep = $nextStep;

		return $this;
	}

	/**
	 * Get comment
	 *
	 * @return string
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * Set comment
	 *
	 * @param string $comment
	 *
	 * @return Casting
	 */
	public function setComment($comment) {
		$this->comment = $comment;

		return $this;
	}
}

