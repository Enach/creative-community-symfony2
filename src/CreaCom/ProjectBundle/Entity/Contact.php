<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table(name="wbs_contact")
 * @ORM\Entity
 */
class Contact {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="image", type="string", length=255)
	 */
	private $image;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lien", type="string", length=255)
	 */
	private $lien;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="adresse", type="string", length=255)
	 */
	private $adresse;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * @param string $nom
	 */
	public function setNom($nom) {
		$this->nom = $nom;
	}

	/**
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param string $image
	 */
	public function setImage($image) {
		$this->image = $image;
	}

	/**
	 * @return string
	 */
	public function getLien() {
		return $this->lien;
	}

	/**
	 * @param string $lien
	 */
	public function setLien($lien) {
		$this->lien = $lien;
	}

	/**
	 * @return string
	 */
	public function getAdresse() {
		return $this->adresse;
	}

	/**
	 * @param string $adresse
	 */
	public function setAdresse($adresse) {
		$this->adresse = $adresse;
	}
}

