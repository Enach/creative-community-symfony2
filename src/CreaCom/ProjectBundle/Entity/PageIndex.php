<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PageIndex
 *
 * @ORM\Table(name="wbs_index")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\PageIndexRepository")
 */
class PageIndex {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="lesActualites", type="array")
	 */
	private $lesActualites;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return PageIndex
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set lesActualites
	 *
	 * @param array $lesActualites
	 *
	 * @return PageIndex
	 */
	public function setLesActualites($lesActualites) {
		$this->lesActualites = $lesActualites;

		return $this;
	}

	/**
	 * Get lesActualites
	 *
	 * @return array
	 */
	public function getLesActualites() {
		return $this->lesActualites;
	}
}

