<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as FOSUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\MembreRepository")
 * @ORM\Table(name="wbs_membres")
 */
class Membre extends FOSUser {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $pseudo;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $nom;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $prenom;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $skype;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $facebook;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $twitter;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $dateInscription;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\ProjectBundle\Entity\Image", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $image;

	/**
	 * @var ArrayCollection
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Poste", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @JoinTable(name="wbs_membres_postes",
	 *      joinColumns={@JoinColumn(name="membre_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="poste_id", referencedColumnName="id")}
	 * )
	 */
	private $poste;

	/**
	 * @var boolean $staff
	 * @ORM\Column(type="boolean")
	 */
	private $staff;

	/**
	 * @var boolean $remerciement
	 * @ORM\Column(type="boolean")
	 */
	private $remerciement = false;

	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 */
	private $order;


	public function __construct() {
		parent::__construct();
		$this->poste           = new ArrayCollection();
		$this->dateInscription = new \DateTime();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * @return string
	 */
	public function getPrenom() {
		return $this->prenom;
	}

	/**
	 * @return string
	 */
	public function getSkype() {
		return $this->skype;
	}

	/**
	 * @return string
	 */
	public function getTwitter() {
		return $this->twitter;
	}

	/**
	 * @return string
	 */
	public function getFacebook() {
		return $this->facebook;
	}

	public function isStaff() {
		return $this->staff;
	}

	/**
	 * add poste
	 *
	 * @param Poste $poste
	 *
	 * @return Membre
	 */
	public function addPoste(Poste $poste) {
		$this->poste->add($poste);

		return $this;
	}

	/**
	 * remove poste
	 *
	 * @param Poste $poste
	 *
	 * @return Membre
	 */
	public function removePoste(Poste $poste) {
		$this->poste->removeElement($poste);

		return $this;
	}

	/**
	 * Get poste
	 *
	 * @return ArrayCollection
	 */
	public function getPostes() {
		return $this->poste;
	}

	/**
	 * @return mixed
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param mixed $image
	 */
	public function setImage($image) {
		$this->image = $image;
	}


	/**
	 * @return string
	 */
	public function getPseudo() {
		return $this->pseudo;
	}

	/**
	 * @param string $pseudo
	 */
	public function setPseudo($pseudo) {
		$this->pseudo = $pseudo;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateInscription() {
		return $this->dateInscription;
	}

	/**
	 * @param \DateTime $dateInscription
	 */
	public function setDateInscription($dateInscription) {
		$this->dateInscription = $dateInscription;
	}

	/**
	 * @return boolean
	 */
	public function isRemerciement() {
		return $this->remerciement;
	}

	/**
	 * @param boolean $remerciement
	 */
	public function setRemerciement($remerciement) {
		$this->remerciement = $remerciement;
	}

	/**
	 * @return int
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * @param int $order
	 */
	public function setOrder($order) {
		$this->order = $order;
	}
}