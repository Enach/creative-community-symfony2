<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * Project
 *
 * @ORM\Table(name="wbs_project")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\ProjectRepository")
 */
class Project {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="slug", type="string", length=255)
	 */
	private $slug;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="video", type="string", length=255, nullable=true)
	 */
	private $video;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="sortie", type="string", length=255, nullable=true)
	 */
	private $sortie;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\TypeProject", fetch="EXTRA_LAZY", inversedBy="projects")
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="resume", type="text", nullable=true)
	 */
	private $resume;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="banniere", type="string", length=255, nullable=true)
	 */
	private $banniere;
	/**
	 * @var ArrayCollection
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Membre", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="wbs_membres_projet",
	 *      joinColumns={@JoinColumn(name="project_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="membre_id", referencedColumnName="id")}
	 * )
	 */
	private $responsables;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\ProjectBundle\Entity\Saison", fetch="EXTRA_LAZY", mappedBy="project")
	 * @ORM\OrderBy({"numero" = "ASC"})
	 */
	private $saisons;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\ProjectBundle\Entity\Actualite", fetch="EXTRA_LAZY", mappedBy="projects")
	 * @ORM\OrderBy({"numero" = "DESC"})
	 */
	private $actualite;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\ProjectBundle\Entity\Roles", fetch="EXTRA_LAZY", mappedBy="project")
	 */
	private $roles;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="public", type="boolean")
	 */
	private $public = false;

	/**
	 * @var string
	 * @ORM\Column(name="background_url", type="string", nullable=true)
	 */
	private $background;

	/**
	 * @var string
	 * @ORM\Column(name="ext_lien", type="string", nullable=true)
	 */
	private $ext_lien;

	/**
	 * @var integer
	 * @ORM\Column(name="order", type="integer")
	 */
	private $order;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="type_serie", type="boolean")
	 */
	private $serie;

	/**
	 * Project constructor.
	 */
	public function __construct() {
		$this->saisons      = new ArrayCollection();
		$this->actualite    = new ArrayCollection();
		$this->roles        = new ArrayCollection();
		$this->responsables = new ArrayCollection();
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Project
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 *
	 * @return Project
	 */
	public function setType($type) {
		$this->type = $type;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Project
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get banniere
	 *
	 * @return string
	 */
	public function getBanniere() {
		return $this->banniere;
	}

	/**
	 * Set banniere
	 *
	 * @param string $banniere
	 *
	 * @return Project
	 */
	public function setBanniere($banniere) {
		$this->banniere = $banniere;

		return $this;
	}

	/**
	 * Get responsable
	 *
	 * @return string
	 */
	public function getResponsables() {
		return $this->responsables;
	}

	/**
	 * add responsable
	 *
	 * @param Membre $responsable
	 *
	 * @return Project
	 */
	public function addResponsable(Membre $responsable) {
		$this->responsables->add($responsable);

		return $this;
	}

	/**
	 * remove responsable
	 *
	 * @param Membre $responsable
	 *
	 * @return Project
	 */
	public function removeResponsable(Membre $responsable) {
		$this->responsables->removeElement($responsable);

		return $this;
	}

	/**
	 * Add saisons
	 *
	 * @param Saison $saison
	 *
	 * @return Project
	 */
	public function addSaison(Saison $saison) {
		$this->saisons->add($saison);

		return $this;
	}

	/**
	 * Remove saisons
	 *
	 * @param Saison $saison
	 *
	 * @return Project
	 */
	public function removeSaison(Saison $saison) {
		$this->saisons->removeElement($saison);

		return $this;
	}

	/**
	 * Get saisons
	 *
	 * @return ArrayCollection
	 */
	public function getSaisons() {
		return $this->saisons;
	}

	/**
	 * Add actualite
	 *
	 * @param Actualite $actualite
	 *
	 * @return Project
	 */
	public function addActualite(Actualite $actualite) {
		$this->actualite->add($actualite);

		return $this;
	}

	/**
	 * Remove actualite
	 *
	 * @param Actualite $actualite
	 *
	 * @return Project
	 */
	public function removeActualite(Actualite $actualite) {
		$this->actualite->removeElement($actualite);

		return $this;
	}

	/**
	 * Get actualite
	 *
	 * @return ArrayCollection
	 */
	public function getActualites() {
		return $this->actualite;
	}

	/**
	 * @return boolean
	 */
	public function isPublic() {
		return $this->public;
	}

	/**
	 * @param boolean $public
	 */
	public function setPublic($public) {
		$this->public = $public;
	}

	/**
	 * Add role
	 *
	 * @param Roles $role
	 *
	 * @return Project
	 */
	public function addRole(Roles $role) {
		$this->roles->add($role);

		return $this;
	}

	/**
	 * Remove role
	 *
	 * @param Roles $role
	 *
	 * @return Project
	 */
	public function removeRole(Roles $role) {
		$this->roles->removeElement($role);

		return $this;
	}

	/**
	 * Get Roles
	 *
	 * @return ArrayCollection
	 */
	public function getRoles() {
		return $this->roles;
	}

	/**
	 * @return string
	 */
	public function getBackground() {
		return $this->background;
	}

	/**
	 * @param string $bgUrl
	 *
	 * @return $this
	 */
	public function setBackground($bgUrl) {
		$this->background = $bgUrl;

		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isSerie() {
		return $this->serie;
	}

	/**
	 * @return mixed
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @param mixed $slug
	 */
	public function setSlug($slug) {
		$this->slug = $slug;
	}

	/**
	 * @return mixed
	 */
	public function getVideo() {
		return $this->video;
	}

	/**
	 * @param mixed $video
	 */
	public function setVideo($video) {
		$this->video = $video;
	}

	/**
	 * @return mixed
	 */
	public function getSortie() {
		return $this->sortie;
	}

	/**
	 * @param mixed $sortie
	 */
	public function setSortie($sortie) {
		$this->sortie = $sortie;
	}

	/**
	 * @return string
	 */
	public function getExtLien() {
		return $this->ext_lien;
	}

	/**
	 * @param string $ext_lien
	 */
	public function setExtLien($ext_lien) {
		$this->ext_lien = $ext_lien;
	}

	/**
	 * @return int
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * @param int $order
	 */
	public function setOrder($order) {
		$this->order = $order;
	}

	/**
	 * @return string
	 */
	public function getResume() {
		return $this->resume;
	}

	/**
	 * @param string $resume
	 */
	public function setResume($resume) {
		$this->resume = $resume;
	}
}

