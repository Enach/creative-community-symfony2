<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Actualite
 *
 * @ORM\Table(name="wbs_actualite")
 * @ORM\Entity
 */
class Actualite {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string", length=255)
	 */
	private $titre;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero", type="integer")
	 */
	private $numero;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text")
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\COlumn(name="image", type="string", length=255)
	 */
	private $image;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lien", type="string", length=255)
	 */
	private $lien;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dateSortie", type="datetime", nullable=true)
	 */
	private $dateSortie;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Project", fetch="EXTRA_LAZY", inversedBy="actualite")
	 */
	private $projects;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get titre
	 *
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	public function getImage() {
		return $this->image;
	}

	/**
	 * Set dateSortie
	 *
	 * @param \DateTime $dateSortie
	 *
	 * @return Actualite
	 */
	public function setDateSortie($dateSortie) {
		$this->dateSortie = $dateSortie;

		return $this;
	}

	/**
	 * Get dateSortie
	 *
	 * @return \DateTime
	 */
	public function getDateSortie() {
		return $this->dateSortie;
	}

	/**
	 * Get lien
	 *
	 * @return string
	 */
	public function getLien() {
		return $this->lien;
	}

	/**
	 * @return Project
	 */
	public function getProject() {
		return $this->projects;
	}

	/**
	 * @param Project $project
	 *
	 * @return $this
	 */
	public function setProject(Project $project) {
		$this->projects = $project;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getNumero() {
		return $this->numero;
	}

	/**
	 * @param int $numero
	 */
	public function setNumero($numero) {
		$this->numero = $numero;
	}
}

