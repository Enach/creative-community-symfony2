<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contribution
 *
 * @ORM\Table(name="wbs_contributions")
 * @ORM\Entity"CreaCom\ProjectBundle\Entity\ContributionRepository")
 */
class Contribution {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255)
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="anneeDebut", type="string", length=4)
	 */
	private $anneeDebut;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="anneeFin", type="string", length=4, nullable=true)
	 */
	private $anneeFin;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="image", type="string", length=255)
	 */
	private $image;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set nom
	 *
	 * @param string $nom
	 *
	 * @return Contribution
	 */
	public function setNom($nom) {
		$this->nom = $nom;

		return $this;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set anneeDebut
	 *
	 * @param string $anneeDebut
	 *
	 * @return Contribution
	 */
	public function setAnneeDebut($anneeDebut) {
		$this->anneeDebut = $anneeDebut;

		return $this;
	}

	/**
	 * Set anneeFin
	 *
	 * @param string $anneeFin
	 *
	 * @return Contribution
	 */
	public function setAnneeFin($anneeFin) {
		$this->anneeFin = $anneeFin;

		return $this;
	}

	/**
	 * Get anneeDebut
	 *
	 * @return string
	 */
	public function getAnneeDebut() {
		return $this->anneeDebut;
	}

	/**
	 * Get anneeFin
	 *
	 * @return string
	 */
	public function getAnneeFin() {
		return $this->anneeFin;
	}

	/**
	 * Set image
	 *
	 * @param string $image
	 *
	 * @return Contribution
	 */
	public function setImage($image) {
		$this->image = $image;

		return $this;
	}

	/**
	 * Get image
	 *
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type) {
		$this->type = $type;
	}
}

