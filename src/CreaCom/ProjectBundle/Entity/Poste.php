<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poste
 *
 * @ORM\Table(name="wbs_postes")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\PosteRepository")
 */
class Poste {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="resume", type="string", length=511)
	 */
	private $resume;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=2047)
	 */
	private $description;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="position", type="integer")
	 */
	private $position;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="logo", type="string", length=255)
	 */
	private $logo;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="statut", type="boolean")
	 */
	private $statut;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="show", type="boolean")
	 */
	private $show = true;

	/**
	 * @var boolean
	 * @ORM\Column(name="formation", type="boolean")
	 */
	private $formation;

	/**
	 * @var string
	 * @ORM\Column(name="lienFormation", type="string", length=255, nullable=true)
	 */
	private $lienFormation;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Get resume
	 *
	 * @return string
	 */
	public function getResume() {
		return $this->resume;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Get logo
	 *
	 * @return string
	 */
	public function getLogo() {
		return $this->logo;
	}

	/**
	 * Set statut
	 *
	 * @param string $statut
	 *
	 * @return Poste
	 */
	public function setStatut($statut) {
		$this->statut = $statut;

		return $this;
	}

	/**
	 * Get statut
	 *
	 * @return string
	 */
	public function getStatut() {
		return $this->statut;
	}

	/**
	 * Get formation
	 *
	 * @return boolean
	 */
	public function getFormation() {
		return $this->formation;
	}


	/**
	 * Get lienFormation
	 *
	 * @return string
	 */
	public function getLienFormation() {
		return $this->lienFormation;
	}

	/**
	 * @return boolean
	 */
	public function isShow() {
		return $this->show;
	}

	/**
	 * @param boolean $show
	 */
	public function setShow($show) {
		$this->show = $show;
	}

	/**
	 * @return int
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * @param int $position
	 */
	public function setPosition($position) {
		$this->position = $position;
	}
}

