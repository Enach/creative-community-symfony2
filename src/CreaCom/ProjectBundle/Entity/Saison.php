<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Saison
 *
 * @ORM\Table(name="project_saison")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\SaisonRepository")
 */
class Saison {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero", type="integer", nullable=true)
	 */
	private $numero;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Project", fetch="EXTRA_LAZY", inversedBy="saisons")
	 */
	private $project;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\ProjectBundle\Entity\Episode", fetch="EXTRA_LAZY", mappedBy="saison")
	 * @ORM\OrderBy({"numero" = "DESC"})
	 */
	private $episodes;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deadline", type="date", nullable=true)
	 */
	private $deadline;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="public", type="boolean")
	 */
	private $public;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="finie", type="boolean")
	 */
	private $finie;

	/**
	 * @var string
	 * @ORM\Column(name="background_url", type="string")
	 */
	private $background;

	/**
	 * Saison constructor.
	 */
	public function __construct() {
		$this->episodes = new ArrayCollection();
		$this->public   = false;
		$this->finie    = false;
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set numero
	 *
	 * @param integer $numero
	 *
	 * @return $this
	 */
	public function setNumero($numero) {
		$this->numero = $numero;

		return $this;
	}

	/**
	 * Get numero
	 *
	 * @return integer
	 */
	public function getNumero() {
		return $this->numero;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return $this
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $desc
	 *
	 * @return $this
	 */
	public function setDescription($desc) {
		$this->description = $desc;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Add episodes
	 *
	 * @param Episode $episodes
	 *
	 * @return Saison
	 */
	public function addEpisode(Episode $episodes) {
		$this->episodes->add($episodes);

		return $this;
	}

	/**
	 * Remove episodes
	 *
	 * @param Episode $episodes
	 *
	 * @return Saison
	 */
	public function removeEpisode(Episode $episodes) {
		$this->episodes->removeElement($episodes);

		return $this;
	}

	/**
	 * Get episodes
	 *
	 * @return ArrayCollection
	 */
	public function getEpisodes() {
		return $this->episodes;
	}

	/**
	 * Set deadline
	 *
	 * @param \DateTime $deadline
	 *
	 * @return Saison
	 */
	public function setDeadline($deadline) {
		$this->deadline = $deadline;

		return $this;
	}

	/**
	 * Get deadline
	 *
	 * @return \DateTime
	 */
	public function getDeadline() {
		return $this->deadline;
	}

	/**
	 * @return boolean
	 */
	public function isPublic() {
		return $this->public;
	}

	/**
	 * @param boolean $public
	 *
	 * @return $this
	 */
	public function setPublic($public) {
		$this->public = $public;

		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isFinie() {
		return $this->finie;
	}

	/**
	 * @param boolean $finie
	 *
	 * @return $this
	 */
	public function setFinie($finie) {
		$this->finie = $finie;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getBackground() {
		return $this->background;
	}

	/**
	 * @param string $bgUrl
	 *
	 * @return $this
	 */
	public function setBackground($bgUrl) {
		$this->background = $bgUrl;

		return $this;
	}

	/**
	 * @return Project
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * @param Project $project
	 *
	 * @return $this
	 */
	public function setProject(Project $project) {
		$this->project = $project;

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getTournages() {
		$lesTournages   = new ArrayCollection();
		$EpisodeCourant = new ArrayCollection();
		foreach ($this->episodes as $EpisodeCourant) {
			foreach ($EpisodeCourant->getTournages() as $TournageCourant) {
				if (!$lesTournages->contains($TournageCourant))
					$lesTournages->add($TournageCourant);
			}
		}

		return $lesTournages;
	}
}