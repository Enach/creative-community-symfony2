<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HelpRequest
 *
 * @ORM\Table(name="wbs_requests")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\HelpRequestRepository")
 */
class HelpRequest {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255)
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="organisme", type="string", length=255, nullable=true)
	 */
	private $organisme;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lieu", type="string", length=255, nullable=true)
	 */
	private $lieu;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dates", type="string", length=255, nullable=true)
	 */
	private $dates;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Ateliers", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $atelier_type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nb_voix", type="string", length=255, nullable=true)
	 */
	private $nbVoix;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="message", type="text", nullable=true)
	 */
	private $message;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="more", type="text", nullable=true)
	 */
	private $more;

	public function __construct() {
		$this->date = new \DateTime();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return HelpRequest
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set nom
	 *
	 * @param string $nom
	 *
	 * @return HelpRequest
	 */
	public function setNom($nom) {
		$this->nom = $nom;

		return $this;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 *
	 * @return HelpRequest
	 */
	public function setEmail($email) {
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set message
	 *
	 * @param string $message
	 *
	 * @return HelpRequest
	 */
	public function setMessage($message) {
		$this->message = $message;

		return $this;
	}

	/**
	 * Get message
	 *
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getNbVoix() {
		return $this->nbVoix;
	}

	/**
	 * @param string $nbVoix
	 */
	public function setNbVoix($nbVoix) {
		$this->nbVoix = $nbVoix;
	}

	/**
	 * @return string
	 */
	public function getMore() {
		return $this->more;
	}

	/**
	 * @param string $more
	 */
	public function setMore($more) {
		$this->more = $more;
	}

	/**
	 * @return string
	 */
	public function getLieu() {
		return $this->lieu;
	}

	/**
	 * @param string $lieu
	 */
	public function setLieu($lieu) {
		$this->lieu = $lieu;
	}

	/**
	 * @return string
	 */
	public function getDates() {
		return $this->dates;
	}

	/**
	 * @param string $dates
	 */
	public function setDates($dates) {
		$this->dates = $dates;
	}

	/**
	 * @return Ateliers
	 */
	public function getAtelierType() {
		return $this->atelier_type;
	}

	/**
	 * @param Ateliers $atelier_type
	 */
	public function setAtelierType(Ateliers $atelier_type) {
		$this->atelier_type = $atelier_type;
	}

	/**
	 * @return string
	 */
	public function getOrganisme() {
		return $this->organisme;
	}

	/**
	 * @param string $organisme
	 */
	public function setOrganisme($organisme) {
		$this->organisme = $organisme;
	}
}

