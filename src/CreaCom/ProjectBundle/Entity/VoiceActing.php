<?php

namespace CreaCom\ProjectBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * VoiceActing
 *
 * @ORM\Table(name="episode_voice_acting")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\VoiceActingRepository")
 */
class VoiceActing {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Episode", fetch="EXTRA_LAZY", inversedBy="voiceActing")
	 */
	private $episode;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(name="resp_voice_acting")
	 */
	private $responsable;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\ProjectBundle\Entity\Casting", fetch="EXTRA_LAZY", mappedBy="voiceActing")
	 */
	private $castings;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deadline", type="date", nullable=true)
	 */
	private $deadline;

	/**
	 * VoiceActing constructor.
	 */
	public function __construct() {
		$this->castings = new ArrayCollection();
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get responsable
	 *
	 * @return User
	 */
	public function getResponsable() {
		return $this->responsable;
	}

	/**
	 * Set responsable
	 *
	 * @param User $responsable
	 *
	 * @return VoiceActing
	 */
	public function setResponsable(Membre $responsable) {
		$this->responsable = $responsable;

		return $this;
	}

	/**
	 * Add casting
	 *
	 * @param Casting $casting
	 *
	 * @return VoiceActing
	 */
	public function addCasting(Casting $casting) {
		$this->castings->add($casting);

		return $this;
	}

	/**
	 * Remove casting
	 *
	 * @param Casting $casting
	 *
	 * @return VoiceActing
	 */
	public function removeCasting(Casting $casting) {
		$this->castings->removeElement($casting);

		return $this;
	}

	/**
	 * Get casting
	 *
	 * @return Casting
	 */
	public function getCastings() {
		return $this->castings;
	}

	/**
	 * Get deadline
	 *
	 * @return \DateTime
	 */
	public function getDeadline() {
		return $this->deadline;
	}

	/**
	 * Set deadline
	 *
	 * @param \DateTime $deadline
	 *
	 * @return VoiceActing
	 */
	public function setDeadline($deadline) {
		$this->deadline = $deadline;

		return $this;
	}

	public function getName() {
		return $this->getEpisode()->getName();
	}

	/**
	 * Get episode
	 *
	 * @return Episode
	 */
	public function getEpisode() {
		return $this->episode;
	}

	/**
	 * Set episode
	 *
	 * @param Episode $episode
	 *
	 * @return VoiceActing
	 */
	public function setEpisode(Episode $episode) {
		$this->episode = $episode;

		return $this;
	}
}

