<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Image
 *
 * @ORM\Entity()
 * @ORM\Table(name="wbs_image")
 * @ORM\HasLifecycleCallbacks
 */
class Image {
	/**
	 * @var file $file
	 */
	public $file;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	/**
	 * @var string $link
	 * @ORM\Column(name="link", type="string", length=255, nullable=true)
	 */
	private $link;
	/**
	 * @var string $name
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	public function getFile() {
		return $this->file;
	}

	public function setFile($file) {
		$this->file = $file;

		return $this;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get link
	 *
	 * @return string
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * Set link
	 *
	 * @param string $link
	 *
	 * @return Image
	 */
	public function setLink($link) {
		$this->link = $link;

		return $this;
	}

	/**
	 * Get $Name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set Name
	 *
	 * @param string $name
	 *
	 * @return Image
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	public function getWebLink() {
		return null === $this->link ? null : $this->getUploadDir() . '/' . $this->link;
	}

	public function getUploadDir() {
		return 'uploads/images';
	}

	public function preUpload() {
		if (null !== $this->file) {

			$filename = sha1(uniqid(mt_rand(), true));

			$this->link = $filename . '.' . $this->file->guessExtension();
		}
	}

	public function upload() {
		/** @var File $file */
		$file = $this->file;
		$file->move($this->getUploadRootDir(), $this->link);

		unset($this->file);
	}

	public function getUploadRootDir() {
		// the absolute directory link where uploaded
		// documents should be saved
		return __DIR__ . '/../../../../web/' . $this->getUploadDir();
	}

	/**
	 * //@ ORM\PostRemove()
	 */
	public function removeUpload() {
		if ($this->file === $this->getAbsoluteLink()) {
			unlink($this->file);
		}
	}

	public function getAbsoluteLink() {
		return null === $this->link ? null : $this->getUploadRootDir() . '/' . $this->link;
	}

	public function __toString() {
		return strval($this->getId());
	}
}
