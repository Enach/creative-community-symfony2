<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roles
 *
 * @ORM\Table(name="project_roles")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\RolesRepository")
 */
class Roles {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="recurrant", type="boolean")
	 */
	private $recurrant = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="revenant", type="boolean")
	 */
	private $revenant = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ponctuel", type="boolean", nullable=true)
	 */
	private $ponctuel = false;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Project", fetch="EXTRA_LAZY", inversedBy="roles")
	 */
	private $project;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Roles
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get recurrant
	 *
	 * @return boolean
	 */
	public function getRecurrant() {
		return $this->recurrant;
	}

	/**
	 * Set recurrant
	 *
	 * @param boolean $recurrant
	 *
	 * @return Roles
	 */
	public function setRecurrant($recurrant) {
		$this->recurrant = $recurrant;

		return $this;
	}

	/**
	 * Get revenant
	 *
	 * @return boolean
	 */
	public function getRevenant() {
		return $this->revenant;
	}

	/**
	 * Set revenant
	 *
	 * @param boolean $revenant
	 *
	 * @return Roles
	 */
	public function setRevenant($revenant) {
		$this->revenant = $revenant;

		return $this;
	}

	/**
	 * Get ponctuel
	 *
	 * @return boolean
	 */
	public function getPonctuel() {
		return $this->ponctuel;
	}

	/**
	 * Set ponctuel
	 *
	 * @param boolean $ponctuel
	 *
	 * @return Roles
	 */
	public function setPonctuel($ponctuel) {
		$this->ponctuel = $ponctuel;

		return $this;
	}

	/**
	 * Get project
	 *
	 * @return Project
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * Set project
	 *
	 * @param Project $project
	 *
	 * @return Roles
	 */
	public function setProject(Project $project) {
		$this->project = $project;

		return $this;
	}
}

