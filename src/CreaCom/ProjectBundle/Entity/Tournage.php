<?php

namespace CreaCom\ProjectBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project_tournage")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\ProjectRepository")
 */
class Tournage {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Episode", fetch="EXTRA_LAZY")
	 */
	private $episodes;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 */
	private $participants;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lieu", type="string", length=255)
	 */
	private $lieu;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="skins", type="text", nullable=true)
	 */
	private $skins;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="script", type="text", nullable=true)
	 */
	private $script;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="fait", type="boolean")
	 */
	private $fait;


	/**
	 * Project constructor.
	 */
	public function __construct() {
		$this->participants = new ArrayCollection();
		$this->episodes     = new ArrayCollection();
		$this->fait         = false;
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Add episodes
	 *
	 * @param Episode $episodes
	 *
	 * @return Saison
	 */
	public function addEpisode(Episode $episodes) {
		$this->episodes->add($episodes);

		return $this;
	}

	/**
	 * Remove episodes
	 *
	 * @param Episode $episodes
	 *
	 * @return Saison
	 */
	public function removeEpisode(Episode $episodes) {
		$this->episodes->removeElement($episodes);

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param mixed $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * Add episodes
	 *
	 * @param User $participant
	 *
	 * @return Tournage
	 */
	public function addParticipant(User $participant) {
		$this->participants->add($participant);

		return $this;
	}

	/**
	 * Remove episodes
	 *
	 * @param User $participant
	 *
	 * @return Tournage
	 */
	public function removeParticipant(User $participant) {
		$this->participants->removeElement($participant);

		return $this;
	}

	/**
	 * Get episodes
	 *
	 * @return Episode
	 */
	public function getParticipants() {
		return $this->participants;
	}

	/**
	 * @return string
	 */
	public function getLieu() {
		return $this->lieu;
	}

	/**
	 * @param string $lieu
	 */
	public function setLieu($lieu) {
		$this->lieu = $lieu;
	}

	/**
	 * @return string
	 */
	public function getSkins() {
		return $this->skins;
	}

	/**
	 * @param string $skins
	 */
	public function setSkins($skins) {
		$this->skins = $skins;
	}

	/**
	 * @return mixed
	 */
	public function getScript() {
		return $this->script;
	}

	/**
	 * @param mixed $script
	 */
	public function setScript($script) {
		$this->script = $script;
	}

	/**
	 * @return boolean
	 */
	public function isFait() {
		return $this->fait;
	}

	/**
	 * @param boolean $fait
	 */
	public function setFait($fait) {
		$this->fait = $fait;
	}

	/**
	 * Get episodes
	 *
	 * @return Episode
	 */
	public function getEpisodes() {
		return $this->episodes;
	}

	public function getName() {
		$result = "";
		/** @var Episode $episode */
		$i = 0;
		foreach ($this->getEpisodes() as $episode) {
			($i != 0 ? $result .= " | " : true);
			$result .= $episode->getSaison()->getProject()->getName() . ' ( ' . $episode->getSaison()->getName() . ' )' . ' - ' . $episode->getName();
			$i++;
		}

		return $result;
	}
}

