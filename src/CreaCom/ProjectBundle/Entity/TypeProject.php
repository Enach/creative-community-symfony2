<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeProject
 *
 * @ORM\Table(name="type_project")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\TypeProjectRepository")
 */
class TypeProject {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="externe", type="boolean")
	 */
	private $externe = false;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\ProjectBundle\Entity\Project", fetch="EXTRA_LAZY", mappedBy="type")
	 */
	private $projects;

	/**
	 * TypeProject constructor.
	 */
	public function __construct() {
		$this->projects = new ArrayCollection();
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return TypeProject
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set externe
	 *
	 * @param boolean $externe
	 *
	 * @return TypeProject
	 */
	public function setExterne($externe) {
		$this->externe = $externe;

		return $this;
	}

	/**
	 * Get externe
	 *
	 * @return boolean
	 */
	public function getExterne() {
		return $this->externe;
	}

	/**
	 * Add projects
	 *
	 * @param Project $project
	 *
	 * @return TypeProject
	 */
	public function addProject(Project $project) {
		$this->projects->add($project);

		return $this;
	}

	/**
	 * Remove projects
	 *
	 * @param Project $project
	 *
	 * @return TypeProject
	 */
	public function removeProject(Project $project) {
		$this->projects->removeElement($project);

		return $this;
	}

	/**
	 * Get projects
	 *
	 * @return ArrayCollection
	 */
	public function getProjects() {
		return $this->projects;
	}
}

