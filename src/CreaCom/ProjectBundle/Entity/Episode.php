<?php

namespace CreaCom\ProjectBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * Episode
 *
 * @ORM\Table(name="project_episode")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\EpisodeRepository")
 */
class Episode {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero", type="integer")
	 */
	private $numero;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Saison", fetch="EXTRA_LAZY", inversedBy="episodes")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $saison;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\ProjectBundle\Entity\Project", fetch="EXTRA_LAZY", inversedBy="episodes")
	 * @ORM\JoinColumn
	 */
	private $projet;

	/**
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Membre", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="episode_auteurs",
	 *      joinColumns={@JoinColumn(name="episode_auteurs", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
	 * )
	 */
	private $auteurs;

	/**
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Membre", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="episode_realisateurs",
	 *      joinColumns={@JoinColumn(name="episode_realisateurs", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
	 * )
	 */
	private $realisateurs;

	/**
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Membre", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="episode_acteurs",
	 *      joinColumns={@JoinColumn(name="episode_acteurs", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
	 * )
	 */
	private $acteurs;

	/**
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Membre", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="episode_voiceacteurs",
	 *      joinColumns={@JoinColumn(name="episode_voiceacteurs", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
	 * )
	 */
	private $voiceActeurs;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deadline", type="datetime", nullable=true)
	 */
	private $deadline;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
	 */
	private $youtube;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="miniature", type="string", length=255, nullable=true)
	 */
	private $miniature;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="public", type="boolean")
	 */
	private $public;

	/**
	 * @ORM\ManyToMany(targetEntity="CreaCom\ProjectBundle\Entity\Tournage", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 * @JoinTable(name="episode_tournages",
	 *      joinColumns={@JoinColumn(name="episode_tournages", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
	 * )
	 */
	private $tournages;

	/**
	 * Episode constructor.
	 */
	public function __construct() {
		$this->auteurs      = new ArrayCollection();
		$this->realisateurs = new ArrayCollection();
		$this->acteurs      = new ArrayCollection();
		$this->voiceActeurs = new ArrayCollection();
		$this->tournages    = new ArrayCollection();
		$this->public       = false;
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get numero
	 *
	 * @return integer
	 */
	public function getNumero() {
		return $this->numero;
	}

	/**
	 * Set numero
	 *
	 * @param integer $numero
	 *
	 * @return Episode
	 */
	public function setNumero($numero) {
		$this->numero = $numero;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Episode
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 *
	 * @param string $desc
	 *
	 * @return Episode
	 */
	public function setDescription($desc) {
		$this->description = $desc;

		return $this;
	}

	/**
	 * add auteurs
	 *
	 * @param User $auteur
	 *
	 * @return Episode
	 */
	public function addAuteur(User $auteur) {
		$this->auteurs->add($auteur);

		return $this;
	}

	/**
	 * remove auteurs
	 *
	 * @param User $auteur
	 *
	 * @return Episode
	 */
	public function removeAuteur(User $auteur) {
		$this->auteurs->removeElement($auteur);

		return $this;
	}

	/**
	 * Get auteurs
	 *
	 * @return ArrayCollection
	 */
	public function getAuteurs() {
		return $this->auteurs;
	}

	/**
	 * Add realisateurs
	 *
	 * @param User $realisateur
	 *
	 * @return Episode
	 */
	public function addRealisateur($realisateur) {
		$this->realisateurs->add($realisateur);

		return $this;
	}

	/**
	 * Remove realisateurs
	 *
	 * @param User $realisateur
	 *
	 * @return Episode
	 */
	public function removeRealisateur($realisateur) {
		$this->realisateurs->removeElement($realisateur);

		return $this;
	}

	/**
	 * Get realisateurs
	 *
	 * @return ArrayCollection
	 */
	public function getRealisateurs() {
		return $this->realisateurs;
	}

	/**
	 * Add acteur
	 *
	 * @param User $acteur
	 *
	 * @return Episode
	 */
	public function addActeur($acteur) {
		$this->acteurs->add($acteur);

		return $this;
	}

	/**
	 * Remove acteur
	 *
	 * @param User $acteur
	 *
	 * @return Episode
	 */
	public function removeActeur($acteur) {
		$this->acteur->removeElement($acteur);

		return $this;
	}

	/**
	 * Get Acteurs
	 *
	 * @return ArrayCollection
	 */
	public function getActeurs() {
		return $this->acteurs;
	}

	/**
	 * Add voiceActeur
	 *
	 * @param User $vActeur
	 *
	 * @return Episode
	 */
	public function addVoiceActeur($vActeur) {
		$this->voiceActeurs->add($vActeur);

		return $this;
	}

	/**
	 * Remove voiceActeur
	 *
	 * @param User $vActeur
	 *
	 * @return Episode
	 */
	public function removeVoiceActeur($vActeur) {
		$this->voiceActeurs->removeElement($vActeur);

		return $this;
	}

	/**
	 * Get realisateurs
	 *
	 * @return ArrayCollection
	 */
	public function getVoiceActeurs() {
		return $this->voiceActeurs;
	}

	/**
	 * Get deadline
	 *
	 * @return \DateTime
	 */
	public function getDeadline() {
		return $this->deadline;
	}

	/**
	 * Set deadline
	 *
	 * @param \DateTime $deadline
	 *
	 * @return Episode
	 */
	public function setDeadline($deadline) {
		$this->deadline = $deadline;

		return $this;
	}

	/**
	 * Get youtube
	 *
	 * @return string
	 */
	public function getYoutube() {
		return $this->youtube;
	}

	/**
	 * Set youtube
	 *
	 * @param string $youtube
	 *
	 * @return Episode
	 */
	public function setYoutube($youtube) {
		$this->youtube = $youtube;

		return $this;
	}

	/**
	 * Get miniature
	 *
	 * @return string
	 */
	public function getMiniature() {
		return $this->miniature;
	}

	/**
	 * Set miniature
	 *
	 * @param string $miniature
	 *
	 * @return Episode
	 */
	public function setMiniature($miniature) {
		$this->miniature = $miniature;

		return $this;
	}

	/**
	 * Get public
	 *
	 * @return boolean
	 */
	public function getPublic() {
		return $this->public;
	}

	/**
	 * Set public
	 *
	 * @param boolean $public
	 *
	 * @return Episode
	 */
	public function setPublic($public) {
		$this->public = $public;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUniqueName() {
		if ($this->getProjet()->getSerie()) {
			return $this->getProjet()->getName() . ' ' . $this->getSaison()->getName() . ' ' . $this->name;
		} else {
			return $this->name;
		}
	}

	/**
	 * @return Saison
	 */
	public function getSaison() {
		return $this->saison;
	}

	/**
	 * @param Saison $saison
	 */
	public function setSaison(Saison $saison) {
		$this->saison = $saison;
	}

	/**
	 * @param Tournage $tournage
	 */
	public function addTournage($tournage) {
		$this->tournages->add($tournage);
	}

	/**
	 * @param Tournage $tournage
	 */
	public function removeTournage($tournage) {
		$this->tournages->removeElement($tournage);
	}

	/**
	 * @return mixed
	 */
	public function getProjet() {
		return $this->projet;
	}

	/**
	 * @param mixed $projet
	 */
	public function setProjet($projet) {
		$this->projet = $projet;
	}
}

