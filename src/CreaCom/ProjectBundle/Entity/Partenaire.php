<?php

namespace CreaCom\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partenaire
 *
 * @ORM\Table(name="wbs_partenaires")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\PartenaireRepository")
 */
class Partenaire {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=511)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="logo", type="string", length=255)
	 */
	private $logo;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set nom
	 *
	 * @param string $nom
	 *
	 * @return Partenaire
	 */
	public function setNom($nom) {
		$this->nom = $nom;

		return $this;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Partenaire
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set logo
	 *
	 * @param string $logo
	 *
	 * @return Partenaire
	 */
	public function setLogo($logo) {
		$this->logo = $logo;

		return $this;
	}

	/**
	 * Get logo
	 *
	 * @return string
	 */
	public function getLogo() {
		return $this->logo;
	}
}

