<?php

namespace CreaCom\ProjectBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\ORM\Mapping as ORM;

/**
 * VoiceActor
 *
 * @ORM\Table(name="voice_actors")
 * @ORM\Entity(repositoryClass="CreaCom\ProjectBundle\Entity\VoiceActorRepository")
 */
class VoiceActor extends Membre {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="site", type="string", length=255, nullable=true)
	 */
	private $site;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="roles", type="array",nullable=true)
	 */
	private $castings;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_of_start", type="date")
	 */
	private $dateOfStart;

	/**
	 * VoiceActor constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->roles       = new ArrayCollection();
		$this->dateOfStart = new \DateTime();
	}

	/**
	 * Get site
	 *
	 * @return string
	 */
	public function getSite() {
		return $this->site;
	}

	/**
	 * Set site
	 *
	 * @param string $site
	 *
	 * @return VoiceActor
	 */
	public function setSite($site) {
		$this->site = $site;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateOfStart() {
		return $this->dateOfStart;
	}

	/**
	 * @param \DateTime $dateOfStart
	 */
	public function setDateOfStart($dateOfStart) {
		$this->dateOfStart = $dateOfStart;
	}

	/**
	 * @return string
	 */
	public function getCastings() {
		return $this->castings;
	}

	/**
	 * @param string $castings
	 */
	public function setCastings($castings) {
		$this->castings = $castings;
	}
}

