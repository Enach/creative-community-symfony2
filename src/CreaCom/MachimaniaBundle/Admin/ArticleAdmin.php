<?php

namespace CreaCom\MachimaniaBundle\Admin;

use CreaCom\MachimaniaBundle\Form\AimesType;
use CreaCom\MachimaniaBundle\Form\ImageType;
use CreaCom\MachimaniaBundle\Form\ParagraphesType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\FormatterBundle\Formatter\Pool as FormatterPool;
use Sonata\FormatterBundle\Formatter\Pool;

class ArticleAdmin extends Admin {

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('auteur', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Membre',
				'property' => 'nom',
			))
			->add('titre', 'text', array(
				'required' => false,
				'label'    => "Titre",
			))
			->end()
			->with('Informations Générales (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('date', 'datetime', array(
				'label' => "Date",
			))
			->add('tags', new AimesType(), array(
				'label' => "Tags",
			))
			->end()
			->with('Into', array(
				'class' => 'col-md-6',
			))
			->add('intro', 'textarea', array(
				'label' => 'Texte',
				'attr'  => array('class' => 'tinymce'),
			))
			->add('imageIntro', new ImageType(), array(
				'label' => "Image",
			))
			->end()
			->with('Conclusion', array(
				'class' => 'col-md-6',
			))
			->add('conclusion', 'textarea', array(
				'label' => "Texte",
				'attr'  => array('class' => 'tinymce'),
			))
			->add('imageConclusion', new ImageType(), array(
				'label' => "Image",
			))
			->end()
			->with('Corps de l\'article', array(
				'class' => 'col-md-12',
			))
			->add('paragraphes', new ParagraphesType(), array(
				'label' => "Paragraphes",
			))
			->end();
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('titre')
			->add('auteur')
			->add('date');
	}
}