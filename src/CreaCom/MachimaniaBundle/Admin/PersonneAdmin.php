<?php

namespace CreaCom\MachimaniaBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PersonneAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('nom', 'text', array(
				'label'    => "Nom",
				'required' => false,
			))
			->add('pseudo', 'text', array(
				'label' => "Pseudo",
			))
			->add('publicName', 'checkbox', array(
				'label'    => "Affiche publiquement le nom",
				'required' => false,
			))
			->end()
			->with('Informations (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('natio', 'text', array(
				'label' => "Nationalité",
			))
			->add('bio', 'textarea', array(
				'label' => "Biographie",
				'attr'  => array('class' => 'tinymce'),
			));
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('nom')
			->add('pseudo')
			->add('natio');
	}
}