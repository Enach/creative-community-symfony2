<?php

namespace CreaCom\MachimaniaBundle\Admin;

use CreaCom\MachimaniaBundle\Entity\Film;
use CreaCom\MachimaniaBundle\Form\CastingType;
use CreaCom\MachimaniaBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;

class FilmAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part1)', array(
				'class' => 'col-md-4',
			))
			->add('titre', 'text', array(
				'required' => true,
			))
			->add('synopsis', 'textarea', array(
				'label' => "Synopsis",
				'attr'  => array('class' => 'tinymce'),
			))
			->end()
			->with('Informations générales (part2)', array(
				'class' => 'col-md-4',
			))
			->add('lienFilm', 'text', array(
				'label' => "Lien",
			))
			->add('studio', 'text', array(
				'label'    => "Studio",
				'required' => false,
			))
			->add('sortie', 'date', array(
				'label' => "Date de Sortie",
			))
			->end()
			->with('Informations générales (part3)', array(
				'class' => 'col-md-4',
			))
			->add('genre', 'text', array(
				'label' => "Genre",
			))
			->add('jeux', 'entity', array(
				'class'    => 'CreaComMachimaniaBundle:Jeux',
				'property' => 'nom',
			))
			->add('duree', 'time', array(
				'label' => "Durée",
			))
			->add('format', 'text', array(
				'label' => "Format",
			))
			->add('langue', 'text', array(
				'required' => true,
			))
			->end()
			->with('Casting', array(
				'class' => 'col-md-4',
			))
			->add('casting', new CastingType(), array(
				'label' => "Casting",
			))
			->end()
			->with('Images (part 1)', array(
				'class' => 'col-md-4',
			))
			->add('banniere', new ImageType(), array(
				'label' => "Bannière",
			))
			->add('affiche', new ImageType(), array(
				'label' => "Affiche",
			))
			->add('image1', new ImageType(), array(
				'label' => "Image 1",
			))
			->add('image2', new ImageType(), array(
				'label' => "Image 2",
			))
			->add('image3', new ImageType(), array(
				'label' => "Image 3",
			))
			->end()
			->with('Images (part 2)', array(
				'class' => 'col-md-4',
			))
			->add('image4', new ImageType(), array(
				'label' => "Image 4",
			))
			->add('image5', new ImageType(), array(
				'label'    => "Image 5",
				'required' => false,
			))
			->add('bandeAnnonce', 'text', array(
				'label'    => "Lien bande annonce",
				'required' => false,
			))
			->end();
	}

//	Déprécié
//	public function validate(ErrorElement $errorElement,Film $object) {
//		// conditional validation, see the related section for more information
//		if (null === $object->getImage5() && null == $object->getBandeAnnonce()) {
//			// abstract cannot be empty when the post is enabled
//			$errorElement
//				->with('bandeAnnonce')->
//				->addViolation('Veuillez indiquer une bande annonce ou une 5ème image')
//				->end();
//		}
//	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('titre')
			->add('lienFilm')
			->add('genre')
			->add('duree')
			->add('format')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
				),
			));
	}
}