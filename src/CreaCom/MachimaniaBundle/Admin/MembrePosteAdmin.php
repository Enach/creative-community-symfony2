<?php

namespace CreaCom\MachimaniaBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MembrePosteAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('titre', 'text', array(
				'label' => "Titre",
			))
			->add('intro', 'textarea', array(
				'label' => "Intro",
			))
			->add('description', 'textarea', array(
				'label' => "Description",
			))
			->add('etat', 'checkbox', array(
				'required' => false,
				'label'    => "Ouvert",
			));
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('titre')
			->add('intro')
			->add('etat')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
				),
			));
	}
}