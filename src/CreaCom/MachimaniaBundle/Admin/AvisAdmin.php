<?php

namespace CreaCom\MachimaniaBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AvisAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales', array(
				'class' => 'col-md-6',
			))
			->add('auteur', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Membre',
				'property' => 'nom',
			))
			->add('film', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Film',
				'property' => 'titre',
			))
			->add('serie', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Serie',
				'property' => 'titre',
			))
			->end()
			->with('Avis', array(
				'class' => 'col-md-6',
			))
			->add('citation', 'text', array(
				'required' => false,
				'label'    => "citation",
			))
			->add('lien', 'text', array(
				'required' => false,
				'label'    => "lien",
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('auteur')
			->add('citation')
			->add('lien');
	}
}