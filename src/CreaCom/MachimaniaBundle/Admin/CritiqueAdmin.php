<?php

namespace CreaCom\MachimaniaBundle\Admin;

use CreaCom\MachimaniaBundle\Form\AimesType;
use CreaCom\MachimaniaBundle\Form\ImageType;
use CreaCom\MachimaniaBundle\Form\ParagraphesType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CritiqueAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('film', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Film',
				'property' => 'titre',
			))
			->add('saison', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Saison',
				'property' => 'nom',
				'label'    => 'Serie par saison',
			))
			->add('titre', 'text', array(
				'required' => false,
				'label'    => "Titre",
			))
			->end()
			->with('Informations Générales (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('auteur', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Membre',
				'property' => 'nom',
			))
			->add('date', 'date', array(
				'label' => "Date",
			))
			->end()
			->with('Introduction', array(
				'class' => 'col-md-6',
			))
			->add('intro', 'textarea', array(
				'label' => "Texte",
			))
			->add('imageIntro', new ImageType(), array(
				'label' => "Image",
			))
			->end()
			->with('Conclusion', array(
				'class' => 'col-md-6',
			))
			->add('conclusion', 'textarea', array(
				'label' => "Texte",
			))
			->add('imageConclusion', new ImageType(), array(
				'label' => "Image",
			))
			->end()
			->with('Corps de la critique', array(
				'class' => 'col-md-6',
			))
			->add('paragraphes', new ParagraphesType(), array(
				'label' => "Paragraphes",
			))
			->end()
			->with('Bilan', array(
				'class' => 'col-md-6',
			))
			->add('aime', new AimesType(), array(
				'label' => "Aime",
			))
			->add('pasaime', new AimesType(), array(
				'label' => "Pas Aimé",
			))
			->end()
			->with('Note', array(
				'class' => 'col-md-6',
			))
			->add('note', 'choice', array(
				'choices'           => array(
					0  => '0',
					1  => '0,5',
					2  => '1',
					3  => '1,5',
					4  => '2',
					5  => '2,5',
					6  => '3',
					7  => '3,5',
					8  => '4',
					9  => '4,5',
					10 => '5',
				),
				'preferred_choices' => array(0),
				'label'             => false,
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('titre')
			->add('auteur')
			->add('date')
			->add('note')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
				),
			));
	}
}