<?php

namespace CreaCom\MachimaniaBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Validator\Constraints\GreaterThan;

class SaisonAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		// only show the child form if this is not itself a child form
		if (!is_numeric($formMapper->getFormBuilder()->getForm()->getName())) {
			$formMapper
				->add('number', 'number', array(
					'label'       => "Numéro",
					'constraints' => array(
						new GreaterThan(0)
					)
				))
				->add('episodes', 'sonata_type_collection', array(
					'type_options' => array(
						// Prevents the "Delete" option from being displayed
						'delete'         => false,
						'delete_options' => array(
							// You may otherwise choose to put the field but hide it
							'type'         => 'hidden',
							// In that case, you need to fill in the options as well
							'type_options' => array(
								'mapped'   => false,
								'required' => false,
							),
						),
					),
				), array(
					'edit'     => 'inline',
					'inline'   => 'table',
					'sortable' => 'position',
				));
		}
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('serie')
			->add('number')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
				),
			));

	}
}