<?php

namespace CreaCom\MachimaniaBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MembreAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Idendité', array(
				'class' => 'col-md-6',
			))
			->add('user', 'entity', array(
				'required' => false,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
				'label'    => 'Utilisateur',
			))
			->add('nom', 'text', array(
				'label'    => "Nom",
				'required' => false,
			))
			->add('pseudoMinecraft', 'text', array(
				'label'    => "Pseudo Minecraft",
				'required' => false,
			))
			->end()
			->with('Informations complémentaires', array(
				'class' => 'col-md-6',
			))
			->add('poste', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:MembrePoste',
				'property' => 'titre',
				'label'    => "Poste",
			))
			->add('citation', 'textarea', array(
				'label' => "Citation",
			))
			->end()
			->with('Réseaux Sociaux', array(
				'class' => 'col-md-6',
			))
			->add('twitter', 'text', array(
				'label'    => "Twitter",
				'required' => false,
			))
			->add('facebook', 'text', array(
				'label'    => "Facebook",
				'required' => false,
			))
			->add('vine', 'text', array(
				'label'     => "Vine",
				'requiered' => false
			))
			->add('instagram', 'text', array(
				'label'     => "Instagram",
				'requiered' => false
			))
			->end()
			->with('Productions', array(
				'class' => 'col-md-6',
			))
			->add('youtube', 'text', array(
				'label'     => "Youtube",
				'requiered' => false
			))
			->add('soundcloud', 'text', array(
				'label'     => "Soundcloud",
				'requiered' => false
			))
			->add('newsground', 'text', array(
				'label'     => "Newsground",
				'requiered' => false
			))
			->end();
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('nom')
			->add('poste')
			// add custom action links
			->add('_action', 'actions', array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
				),
			));
	}
}