<?php

namespace CreaCom\MachimaniaBundle\Admin;

use CreaCom\MachimaniaBundle\Form\ParagrapheType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AvisSpectateursAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('titre', 'text', array(
				'required' => false,
				'label'    => "Titre",
			))
			->add('date', 'datetime', array(
				'label' => "Date",
			))
			->add('critique', new ParagrapheType(), array(
				'label' => "Critique",
			))
			->add('note', 'number', array(
				'label' => "Note",
			));
	}

	// Fields to be shown on lists
	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('titre')
			->add('date')
			->add('note');
	}
}