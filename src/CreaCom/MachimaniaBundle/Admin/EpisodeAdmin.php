<?php

namespace CreaCom\MachimaniaBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Validator\Constraints\GreaterThan;

class EpisodeAdmin extends Admin {
	// Fields to be shown on create/edit forms
	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Informations Générales (part 1)', array(
				'class' => 'col-md-6',
			))
			->add('saison', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Saison',
				'property' => 'number',
			))
			->add('numero', 'number', array(
				'label'       => "Numéro",
				'constraints' => array(
					new GreaterThan(0)
				)
			))
			->add('titre', 'text', array(
				'label' => "Titre",
			))
			->end()
			->with('Informations Générales (part 2)', array(
				'class' => 'col-md-6',
			))
			->add('resume', 'textarea', array(
				'attr'  => array('class' => 'tinymce'),
				'label' => "Résumé",
			))
			->add('duree', 'text', array(
				'label' => "Durée",
			))
			->add('diffusion', 'datetime', array(
				'label' => "Diffusion",
			))
			->end();
	}
}