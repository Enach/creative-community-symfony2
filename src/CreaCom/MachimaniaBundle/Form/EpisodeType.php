<?php

namespace CreaCom\MachimaniaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;

class EpisodeType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('numero', 'number', array(
				'label'       => "Numéro",
				'constraints' => array(
					new GreaterThan(0)
				)
			))
			->add('titre', 'text', array(
				'label' => "Titre",
			))
			->add('resume', 'textarea', array(
				'attr'  => array('class' => 'tinymce'),
				'label' => "Numéro",
			))
			->add('duree', 'text', array(
				'label' => "Durée",
			))
			->add('diffusion', 'date', array(
				'label' => "Diffusion",
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'CreaCom\MachimaniaBundle\Entity\Episode',
			'user'       => null,
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'creacom_machimaniabundle_episode';
	}
}
