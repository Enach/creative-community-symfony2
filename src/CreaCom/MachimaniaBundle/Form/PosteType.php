<?php

namespace CreaCom\MachimaniaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PosteType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('personne', 'entity', array(
				'required' => false,
				'class'    => 'CreaComMachimaniaBundle:Personne',
				'property' => 'nom',
			))
			->add('role', 'text', array(
				'label' => "Role",
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'CreaCom\MachimaniaBundle\Entity\Poste',
			'user'       => null,
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'creacom_machimaniabundle_poste';
	}
}
