<?php

namespace CreaCom\MachimaniaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParagraphesType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('realisation', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new ParagrapheType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => false,
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array());
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'creacom_machimaniabundle_paragraphes';
	}
}
