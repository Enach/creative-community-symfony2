<?php

namespace CreaCom\MachimaniaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParagrapheType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('titre', 'text', array(
				'label' => "Titre",
			))
			->add('img', new ImageType(), array(
				'label' => "Image",
			))
			->add('content', 'textarea', array(
				'label' => "Contenu",
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'CreaCom\MachimaniaBundle\Entity\Paragraphe',
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'creacom_machimaniabundle_paragraphe';
	}
}
