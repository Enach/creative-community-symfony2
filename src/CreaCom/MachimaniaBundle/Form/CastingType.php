<?php

namespace CreaCom\MachimaniaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CastingType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('realisation', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Equipe de réalisation",
			))
			->add('ecriture', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Equipe d'écriture",
			))
			->add('acteurPrimary', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Acteurs principaux",
			))
			->add('ActeurSecondary', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Acteurs secondaire",
			))
			->add('musique', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Equipe de musique",
			))
			->add('tech', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Equipe technique",
			))
			->add('production', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Producteur",
			))
			->add('parts', 'sonata_type_native_collection', array(
				'required'     => false,
				'type'         => new PosteType(),
				'allow_add'    => true,
				'allow_delete' => true,
				'label'        => "Partenaires",
			))
			->add('background', new ImageType(), array(
				'label' => "Fond d'écran",
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'CreaCom\MachimaniaBundle\Entity\Casting',
			'user'       => null,
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'creacom_machimaniabundle_casting';
	}
}
