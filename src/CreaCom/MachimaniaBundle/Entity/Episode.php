<?php

namespace CreaCom\MachimaniaBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Episode
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_episode")
 */
class Episode {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Saison", inversedBy="episodes")
	 */
	private $saison;

	/**
	 * @var int
	 *
	 * @Assert\Type(
	 *     type="integer",
	 *     message="The value {{ value }} is not a valid {{ type }}."
	 * )
	 *
	 * @ORM\Column(type="integer")
	 */
	private $numero;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $titre;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $resume;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $duree;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $diffusion;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set $saison
	 *
	 * @param Saison $saison
	 *
	 * @return Saison
	 */
	public function setSaison(Saison $saison) {
		$this->saison = $saison;

		return $this;
	}

	/**
	 * Get critique
	 *
	 * @return string
	 */
	public function getSaison() {
		return $this->saison;
	}

	/**
	 * Set titre
	 *
	 * @param string $titre
	 *
	 * @return Episode
	 */
	public function setTitre($titre) {
		$this->titre = $titre;

		return $this;
	}

	/**
	 * Get titre
	 *
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * @return DateTime
	 */
	public function getDiffusion() {
		return $this->diffusion;
	}

	/**
	 * @param DateTime $diffusion
	 */
	public function setDiffusion($diffusion) {
		$this->diffusion = $diffusion;
	}

	/**
	 * @return int
	 */
	public function getNumero() {
		return $this->numero;
	}

	/**
	 * @param int $numero
	 */
	public function setNumero($numero) {
		$this->numero = $numero;
	}

	/**
	 * @return string
	 */
	public function getDuree() {
		return $this->duree;
	}

	/**
	 * @param string $duree
	 */
	public function setDuree($duree) {
		$this->duree = $duree;
	}

	/**
	 * @return string
	 */
	public function getResume() {
		return $this->resume;
	}

	/**
	 * @param string $resume
	 */
	public function setResume($resume) {
		$this->resume = $resume;
	}
}

