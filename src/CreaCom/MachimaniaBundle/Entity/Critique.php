<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Critique
 *
 * @ORM\Table(name="machimania_critique")
 * @ORM\Entity(repositoryClass="CreaCom\MachimaniaBundle\Entity\CritiqueRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Critique {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string", length=255)
	 */
	private $titre;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="intro", type="text")
	 */
	private $intro;

	/**
	 * @var ArrayCollection
	 * @ORM\Column(type="array")
	 */
	private $paragraphes;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="conclusion", type="text")
	 */
	private $conclusion;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Membre", fetch="EXTRA_LAZY", inversedBy="critiques")
	 */
	private $auteur;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="aime", type="array")
	 */
	private $aime;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="pasaime", type="array")
	 */
	private $pasaime;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="note", type="float")
	 */
	private $note;

	/**
	 * @ORM\OneToOne(targetEntity="Film", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $film;

	/**
	 * @ORM\OneToOne(targetEntity="Saison", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $saison;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean")
	 */
	private $label = false;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $imageIntro;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $imageConclusion;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="publied")
	 */
	private $publied = false;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->paragraphes = new ArrayCollection();
		$this->date        = new \DateTime();
		$this->aime        = new ArrayCollection();
		$this->pasaime     = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set titre
	 *
	 * @param string $titre
	 *
	 * @return Critique
	 */
	public function setTitre($titre) {
		$this->titre = $titre;

		return $this;
	}

	/**
	 * Get titre
	 *
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return Critique
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set intro
	 *
	 * @param string $intro
	 *
	 * @return Critique
	 */
	public function setIntro($intro) {
		$this->intro = $intro;

		return $this;
	}

	/**
	 * Get intro
	 *
	 * @return string
	 */
	public function getIntro() {
		return $this->intro;
	}

	/**
	 * Add paragraphes
	 *
	 * @param $paragraphes
	 *
	 * @return Paragraphe
	 */
	public function addParagraphes($paragraphes) {
		$this->paragraphes[] = $paragraphes;

		return $this;
	}

	/**
	 * Remove paragraphes
	 *
	 * @param $paragraphes
	 */
	public function removeParagraphes($paragraphes) {
		$this->paragraphes->removeElement($paragraphes);
	}

	/**
	 * Get paragraphes
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getParagraphes() {
		return $this->paragraphes;
	}

	/**
	 * Set conclusion
	 *
	 * @param string $conclusion
	 *
	 * @return Critique
	 */
	public function setConclusion($conclusion) {
		$this->conclusion = $conclusion;

		return $this;
	}

	/**
	 * Get conclusion
	 *
	 * @return string
	 */
	public function getConclusion() {
		return $this->conclusion;
	}

	/**
	 * Set auteur
	 *
	 * @param Membre $auteur
	 *
	 * @return Critique
	 */
	public function setAuteur(Membre $auteur) {
		$this->auteur = $auteur;

		return $this;
	}

	/**
	 * Get auteur
	 *
	 * @return Membre
	 */
	public function getAuteur() {
		return $this->auteur;
	}

	/**
	 * Add aime
	 *
	 * @param $aime
	 *
	 * @return Paragraphe
	 * @internal param $paragraphes
	 *
	 */
	public function addAime($aime) {
		$this->aime[] = $aime;

		return $this;
	}

	/**
	 * Remove aime
	 *
	 * @param $aime
	 *
	 * @internal param $paragraphes
	 */
	public function removeAime($aime) {
		$this->aime->removeElement($aime);
	}

	/**
	 * Get aime
	 *
	 * @return array
	 */
	public function getAime() {
		return $this->aime;
	}

	/**
	 * Add pasaime
	 *
	 * @param $pasaime
	 *
	 * @return $this
	 */
	public function addPasAime($pasaime) {
		$this->pasaime[] = $pasaime;

		return $this;
	}

	/**
	 * Remove aime
	 *
	 * @param $pasaime
	 */
	public function removePasAime($pasaime) {
		$this->pasaime->removeElement($pasaime);
	}

	/**
	 * Get pasaime
	 *
	 * @return array
	 */
	public function getPasaime() {
		return $this->pasaime;
	}

	/**
	 * Set note
	 *
	 * @param float $note
	 *
	 * @return Critique
	 */
	public function setNote($note) {
		$this->note = $note;

		return $this;
	}

	/**
	 * Get note
	 *
	 * @return float
	 */
	public function getNote() {
		return $this->note;
	}

	/**
	 * Set film
	 *
	 * @param string $film
	 *
	 * @return Critique
	 */
	public function setFilm($film) {
		$this->film = $film;

		return $this;
	}

	/**
	 * Get film
	 *
	 * @return string
	 */
	public function getFilm() {
		return $this->film;
	}

	/**
	 * Set Saison
	 *
	 * @param Saison $saison
	 *
	 * @return Critique
	 */
	public function setSaison($saison) {
		$this->saison = $saison;

		return $this;
	}

	/**
	 * Get Saison
	 *
	 * @return Saison
	 */
	public function getSaison() {
		return $this->saison;
	}

	/**
	 * Set imageIntro
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImageIntro($image) {
		$this->imageIntro = $image;

		return $this;
	}

	/**
	 * Get imageIntro
	 *
	 * @return Image
	 */
	public function getImageIntro() {
		return $this->imageIntro;
	}

	/**
	 * Set imageConclusion
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImageConclusion($image) {
		$this->imageConclusion = $image;

		return $this;
	}

	/**
	 * Get imageConclusion
	 *
	 * @return Image
	 */
	public function getImageConclusion() {
		return $this->imageConclusion;
	}

	/**
	 * @return boolean
	 */
	public function isLabel() {
		return $this->label;
	}

	/**
	 * @param boolean $label
	 */
	public function setLabel($label) {
		$this->label = $label;
	}

	/**
	 * @return boolean
	 */
	public function isPublied() {
		return $this->publied;
	}

	/**
	 * @param boolean $publied
	 */
	public function setPublied($publied) {
		$this->publied = $publied;
	}
}

