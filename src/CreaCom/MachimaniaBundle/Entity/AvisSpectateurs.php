<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Critique
 *
 * @ORM\Entity(repositoryClass="CreaCom\MachimaniaBundle\Entity\AvisSpectateursRepository")
 * @ORM\Table(name="machimania_avis_spectateur")
 * @ORM\HasLifecycleCallbacks
 */
class AvisSpectateurs {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Film", fetch="EXTRA_LAZY", inversedBy="avisSpectateurs")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $film;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Serie", fetch="EXTRA_LAZY", inversedBy="avisSpectateurs")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $serie;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $date;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $titre;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $critique;
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $auteur;

	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer")
	 */
	private $note;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->date = new \DateTime();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getAuteur() {
		return $this->auteur;
	}

	/**
	 * @param string $auteur
	 */
	public function setAuteur($auteur) {
		$this->auteur = $auteur;
	}

	/**
	 * @return Film
	 */
	public function getFilm() {
		return $this->film;
	}

	/**
	 * @param Film $film
	 */
	public function setFilm($film) {
		$this->film = $film;
	}

	/**
	 * @return Serie
	 */
	public function getSerie() {
		return $this->serie;
	}

	/**
	 * @param Serie $serie
	 */
	public function setSerie($serie) {
		$this->serie = $serie;
	}

	/**
	 * @return string
	 */
	public function getCritique() {
		return $this->critique;
	}

	/**
	 * @param string $critique
	 */
	public function setCritique($critique) {
		$this->critique = $critique;
	}

	/**
	 * @return datetime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param date $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * @return int
	 */
	public function getNote() {
		return $this->note;
	}

	/**
	 * @param int $note
	 */
	public function setNote($note) {
		$this->note = $note;
	}

	/**
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * @param string $titre
	 */
	public function setTitre($titre) {
		$this->titre = $titre;
	}
}

