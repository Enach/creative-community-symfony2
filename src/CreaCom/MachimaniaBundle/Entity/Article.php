<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\ClassificationBundle\Entity\Tag;

/**
 * Critique
 *
 * @ORM\Entity(repositoryClass="CreaCom\MachimaniaBundle\Entity\ArticleRepository")
 * @ORM\Table(name="machimania_article")
 */
class Article {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string", length=255)
	 */
	private $titre;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="intro", type="text")
	 */
	private $intro;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Paragraphe", mappedBy="article")
	 */
	private $paragraphes;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="conclusion", type="text")
	 */
	private $conclusion;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255)
	 */
	private $type;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Membre", fetch="EXTRA_LAZY", inversedBy="articles")
	 */
	private $auteur;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array")
	 */
	private $tags;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $imageIntro;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $imageConclusion;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="publied")
	 */
	private $publied = false;

	private $rawContent;

	private $contentFormatter;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->paragraphes = new ArrayCollection();
		$this->date        = new \DateTime();
		$this->tags        = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set titre
	 *
	 * @param string $titre
	 *
	 * @return Critique
	 */
	public function setTitre($titre) {
		$this->titre = $titre;

		return $this;
	}

	/**
	 * Get titre
	 *
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return Critique
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set intro
	 *
	 * @param string $intro
	 *
	 * @return Critique
	 */
	public function setIntro($intro) {
		$this->intro = $intro;

		return $this;
	}

	/**
	 * Get intro
	 *
	 * @return string
	 */
	public function getIntro() {
		return $this->intro;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addTags(Tag $tags) {
		$this->tags[] = $tags;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTags() {
		return $this->tags;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setTags($tags) {
		$this->tags = $tags;
	}

	/**
	 * Add paragraphes
	 *
	 * @param $paragraphes
	 *
	 * @return Paragraphe
	 */
	public function addParagraphes(Paragraphe $paragraphes) {
		$this->paragraphes[] = $paragraphes;

		return $this;
	}

	/**
	 * Remove paragraphes
	 *
	 * @param $paragraphes
	 */
	public function removeParagraphes(Paragraphe $paragraphes) {
		$this->paragraphes->removeElement($paragraphes);
	}

	/**
	 * Get paragraphes
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getParagraphes() {
		return $this->paragraphes;
	}

	/**
	 * Set conclusion
	 *
	 * @param string $conclusion
	 *
	 * @return Critique
	 */
	public function setConclusion($conclusion) {
		$this->conclusion = $conclusion;

		return $this;
	}

	/**
	 * Get conclusion
	 *
	 * @return string
	 */
	public function getConclusion() {
		return $this->conclusion;
	}

	/**
	 * Set auteur
	 *
	 * @param string $auteur
	 *
	 * @return Critique
	 */
	public function setAuteur($auteur) {
		$this->auteur = $auteur;

		return $this;
	}

	/**
	 * Get auteur
	 *
	 * @return string
	 */
	public function getAuteur() {
		return $this->auteur;
	}

	/**
	 * @return mixed
	 */
	public function getImageIntro() {
		return $this->imageIntro;
	}

	/**
	 * @param mixed $imageIntro
	 */
	public function setImageIntro(Image $imageIntro) {
		$this->imageIntro = $imageIntro;
	}

	/**
	 * @return mixed
	 */
	public function getImageConclusion() {
		return $this->imageConclusion;
	}

	/**
	 * @param mixed $imageConclusion
	 */
	public function setImageConclusion(Image $imageConclusion) {
		$this->imageConclusion = $imageConclusion;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * @return boolean
	 */
	public function isPublied() {
		return $this->publied;
	}

	/**
	 * @param boolean $publied
	 */
	public function setPublied($publied) {
		$this->publied = $publied;
	}

	/**
	 * @param $contentFormatter
	 */
	public function setContentFormatter($contentFormatter) {
		$this->contentFormatter = $contentFormatter;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getContentFormatter() {
		return $this->contentFormatter;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRawContent($rawContent) {
		$this->rawContent = $rawContent;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRawContent() {
		return $this->rawContent;
	}
}

