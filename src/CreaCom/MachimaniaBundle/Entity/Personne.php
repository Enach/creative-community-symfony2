<?php

namespace CreaCom\MachimaniaBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Personne
 *
 * @ORM\Table(name="machimania_personne")
 * @ORM\Entity(repositoryClass="CreaCom\MachimaniaBundle\Entity\PersonneRepository")
 */
class Personne {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $pseudo;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $publicName = true;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $natio;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $bio;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $updated;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $castings = null;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->updated = new \DateTime();
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * @param mixed $nom
	 */
	public function setNom($nom) {
		$this->nom = $nom;
	}

	/**
	 * @return string
	 */
	public function getPseudo() {
		return $this->pseudo;
	}

	/**
	 * @param string $pseudo
	 */
	public function setPseudo($pseudo) {
		$this->pseudo = $pseudo;
	}

	/**
	 * @return string
	 */
	public function getNatio() {
		return $this->natio;
	}

	/**
	 * @param string $natio
	 */
	public function setNatio($natio) {
		$this->natio = $natio;
	}

	/**
	 * @return string
	 */
	public function getBio() {
		return $this->bio;
	}

	/**
	 * @param string $bio
	 */
	public function setBio($bio) {
		$this->bio = $bio;
	}

	/**
	 * @return datetime
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * @param datetime $update
	 */
	public function setUpdated(Datetime $update) {
		$this->updated = $update;
	}

	/**
	 * Add castings
	 *
	 * @param Casting $casting
	 *
	 * @return Casting
	 */
	public function addCasting(Casting $casting) {
		$this->castings[] = $casting;

		return $this;
	}

	/**
	 * Remove castings
	 *
	 * @param Casting $casting
	 */
	public function removeCasting(Casting $casting) {
		$this->castings->removeElement($casting);
	}

	/**
	 * Get castings
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCastings() {
		return $this->castings;
	}

	/**
	 * @return boolean
	 */
	public function isPublicName() {
		return $this->publicName;
	}

	/**
	 * @param boolean $publicName
	 */
	public function setPublicName($publicName) {
		$this->publicName = $publicName;
	}

}