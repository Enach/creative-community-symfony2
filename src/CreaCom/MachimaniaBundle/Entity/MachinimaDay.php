<?php

namespace CreaCom\MachimaniaBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * MachinimaDay
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_machinimaDay")
 */
class MachinimaDay {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="day", type="datetime")
	 */
	private $day;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Serie")
	 */
	private $serie;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Film")
	 */
	private $film;

	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return datetime
	 */
	public function getDay() {
		return $this->day;
	}

	/**
	 * @param datetime $day
	 */
	public function setDay(DateTime $day) {
		$this->day = $day;
	}

	/**
	 * @return Film
	 */
	public function getFilm() {
		return $this->film;
	}

	/**
	 * @param Film $film
	 */
	public function setFilm($film) {
		$this->film = $film;
	}

	/**
	 * @return Serie
	 */
	public function getSerie() {
		return $this->serie;
	}

	/**
	 * @param Serie $serie
	 */
	public function setSerie($serie) {
		$this->serie = $serie;
	}
}

