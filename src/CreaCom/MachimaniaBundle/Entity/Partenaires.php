<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partenaires
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_partenaires")
 */
class Partenaires {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string" ,nullable=true)
	 */
	private $nom;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $logo;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $apport;

	/**
	 * Constructor
	 */
	public function __construct() {
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * @param Image $nom
	 */
	public function setNom($nom) {
		$this->nom = $nom;
	}

	/**
	 * @return Image
	 */
	public function getLogo() {
		return $this->logo;
	}

	/**
	 * @param mixed $logo
	 */
	public function setLogo($logo) {
		$this->logo = $logo;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getApport() {
		return $this->apport;
	}

	/**
	 * @param string $apport
	 */
	public function setApport($apport) {
		$this->apport = $apport;
	}
}

