<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Avis
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_avis")
 * @ORM\HasLifecycleCallbacks
 */
class Avis {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Film", fetch="EXTRA_LAZY", inversedBy="avis")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $film;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Film", fetch="EXTRA_LAZY", inversedBy="avis")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $serie;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $citation;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Membre", fetch="EXTRA_LAZY", inversedBy="avis")
	 */
	private $auteur;
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $lien;


	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getCitation() {
		return $this->citation;
	}

	/**
	 * @param string $citation
	 */
	public function setCitation($citation) {
		$this->citation = $citation;
	}

	/**
	 * @return string
	 */
	public function getLien() {
		return $this->lien;
	}

	/**
	 * @param string $lien
	 */
	public function setLien($lien) {
		$this->lien = $lien;
	}

	/**
	 * @return Membre
	 */
	public function getAuteur() {
		return $this->auteur;
	}

	/**
	 * @param Membre $auteur
	 */
	public function setAuteur(Membre $auteur) {
		$this->auteur = $auteur;
	}

	/**
	 * @return Film
	 */
	public function getFilm() {
		return $this->film;
	}

	/**
	 * @param Film $film
	 */
	public function setFilm($film) {
		$this->film = $film;
	}

	/**
	 * @return Serie
	 */
	public function getSerie() {
		return $this->serie;
	}

	/**
	 * @param Serie $serie
	 */
	public function setSerie($serie) {
		$this->serie = $serie;
	}
}

