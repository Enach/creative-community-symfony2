<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CritiqueRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CritiqueRepository extends EntityRepository {

	public function getNoteGlobale($films, $series) {

		$somme = 0;
		$i     = 0;

		foreach ($films as $film) {
			$critique = $this->createQueryBuilder('t')
				->from('CreaComMachimaniaBundle:AvisSpectateurs', 'avis_spectateurs')
				->where('avis_spectateurs.film = :film')
				->setParameter('film', $film)
				->getQuery()
				->getOneOrNullResult();

			/** @var Critique $critique */
			$somme += $critique->getNote();
			$i++;
		}

		foreach ($series as $serie) {
			$critique = $this->createQueryBuilder('t')
				->from('CreaComMachimaniaBundle:AvisSpectateurs', 'avis_spectateurs')
				->where('t.serie = :serie')
				->setParameter('serie', $serie)
				->getQuery()
				->getOneOrNullResult();

			/** @var Critique $critique */
			$somme += $critique->getNote();
			$i++;
		}
		$moyenne = $somme / $i;
		$moyenne = round($moyenne, 1, PHP_ROUND_HALF_DOWN);

		return $moyenne;
	}

	public function getConseillons($type) {

		if ($type == "saison") {
			$query = $this->createQueryBuilder('a')
				->where('a.label = true')
				->andWhere($query->expr()->isNotNull('a.saison'))
				->andWhere('a.publied = true')
				->orderBy('a.note', 'DESC')
				->setMaxResults('3')
				->getQuery();
		} else {
			$query = $this->createQueryBuilder('a')
				->where('a.label = true')
				->andWhere($query->expr()->isNotNull('a.film'))
				->andWhere('a.publied = true')
				->orderBy('a.note', 'DESC')
				->setMaxResults('3')
				->getQuery();
		}

		$conseillons = $query->getArrayResult();

		return $conseillons;
	}
}
