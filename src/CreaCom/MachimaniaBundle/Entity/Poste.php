<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poste
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_casting_poste")
 */
class Poste {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Casting")
	 */
	private $casting;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Personne")
	 */
	private $personne;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $role;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * @param string $role
	 */
	public function setRole($role) {
		$this->role = $role;
	}

	/**
	 * @return Casting
	 */
	public function getCasting() {
		return $this->casting;
	}

	/**
	 * @param Casting $casting
	 */
	public function setCasting(Casting $casting) {
		$this->casting = $casting;
	}

	/**
	 * @return Personne
	 */
	public function getPersonne() {
		return $this->personne;
	}

	/**
	 * @param Personne $personne
	 */
	public function setPersonne(Personne $personne) {
		$this->personne = $personne;
	}


}

