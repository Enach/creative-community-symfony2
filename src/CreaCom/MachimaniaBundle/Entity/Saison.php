<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Saison
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_saison")
 */
class Saison {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $nom;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="number", type="integer")
	 */
	private $number;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Episode", mappedBy="saison")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $episodes;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Serie", fetch="EXTRA_LAZY",
	 *                                                                      inversedBy="saisons")
	 */
	private $serie;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->episodes = new ArrayCollection();
	}

	/**
	 * @return string
	 */
	public function getNom() {
		/** @var Serie $serie */
		$serie = $this->getSerie();
		if (null !== $serie) {
			$nom = $serie->getTitre();
			$nom .= $this->number;
		} else {
			$nom = $this->nom;
		}

		return $nom;
	}

	/**
	 * @param string $nom
	 */
	public function setNom($nom) {
		$this->nom = $nom;
	}

	/**
	 * Get serie
	 *
	 * @return string
	 */
	public function getSerie() {
		return $this->serie;
	}

	/**
	 * Set serie
	 *
	 * @param Serie $serie
	 *
	 * @return Saison
	 */
	public function setSerie(Serie $serie) {
		$this->serie = $serie;

		return $this;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Add Episode
	 *
	 * @param Episode $episode
	 *
	 * @return Saison
	 */
	public function addEpisodes(Episode $episode) {
		$this->episodes[] = $episode;

		return $this;
	}

	/**
	 * Remove Episode
	 *
	 * @param Episode $episode
	 */
	public function removeEpisodes(Episode $episode) {
		$this->episodes->removeElement($episode);
	}

	/**
	 * Get Episode
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getEpisodes() {
		return $this->episodes;
	}

	/**
	 * @return int
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * @param int $number
	 */
	public function setNumber($number) {
		$this->number = $number;
	}
}

