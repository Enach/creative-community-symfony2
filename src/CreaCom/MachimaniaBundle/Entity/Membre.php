<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Membre
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_membre")
 */
class Membre {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $user;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string" ,nullable=true, length=255)
	 */
	private $nom;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\MembrePoste", fetch="EXTRA_LAZY", inversedBy="membre")
	 * @ORM\JoinColumn(name="poste", referencedColumnName="id",nullable=false)
	 */
	private $poste;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Critique", fetch="EXTRA_LAZY", mappedBy="auteur")
	 */
	private $critiques;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Article", fetch="EXTRA_LAZY", mappedBy="auteur")
	 */
	private $articles;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Avis", fetch="EXTRA_LAZY", mappedBy="auteur")
	 */
	private $avis;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $citation;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $twitter;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $facebook;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $youtube;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $soundcloud;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $vine;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $instagram;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $pseudoMinecraft;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",nullable=true, length=255)
	 */
	private $newsground;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->articles  = new ArrayCollection();
		$this->critiques = new ArrayCollection();
		$this->avis      = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser(User $user) {
		$this->user = $user;
	}

	/**
	 * @return MembrePoste
	 */
	public function getPoste() {
		return $this->poste;
	}

	/**
	 * @param MembrePoste $poste
	 */
	public function setPoste(MembrePoste $poste) {
		$this->poste = $poste;
	}

	/**
	 * @return string
	 */
	public function getCitation() {
		return $this->citation;
	}

	/**
	 * @param string $citation
	 */
	public function setCitation($citation) {
		$this->citation = $citation;
	}

	/**
	 * @return mixed
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * @param mixed $nom
	 */
	public function setNom($nom) {
		$this->nom = $nom;
	}

	/**
	 * Add critiques
	 *
	 * @param Critique $critiques
	 *
	 * @return $this
	 */
	public function addCritiques($critiques) {
		$this->critiques[] = $critiques;

		return $this;
	}

	/**
	 * Remove critiques
	 *
	 * @param Critique $critiques
	 */
	public function removeCritiques($critiques) {
		$this->critiques->removeElement($critiques);
	}

	/**
	 * Get critiques
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCritiques() {
		return $this->critiques;
	}

	/**
	 * Add Articles
	 *
	 * @param Article $articles
	 *
	 * @return $this
	 */
	public function addArticles($articles) {
		$this->articles[] = $articles;

		return $this;
	}

	/**
	 * Remove Articles
	 *
	 * @param Article $articles
	 */
	public function removeArticles($articles) {
		$this->articles->removeElement($articles);
	}

	/**
	 * Get Articles
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getArticles() {
		return $this->articles;
	}

	/**
	 * Add Avis
	 *
	 * @param Avis $avis
	 *
	 * @return $this
	 */
	public function addAvis(Avis $avis) {
		$this->avis[] = $avis;

		return $this;
	}

	/**
	 * Remove Avis
	 *
	 * @param Avis $avis
	 */
	public function removeAvis(Avis $avis) {
		$this->avis->removeElement($avis);
	}

	/**
	 * Get Avis
	 *
	 * @return ArrayCollection|Avis
	 */
	public function getAvis() {
		return $this->avis;
	}

	/**
	 * @return string
	 */
	public function getTwitter() {
		return $this->twitter;
	}

	/**
	 * @param string $twitter
	 */
	public function setTwitter($twitter) {
		$this->twitter = $twitter;
	}

	/**
	 * @return string
	 */
	public function getYoutube() {
		return $this->youtube;
	}

	/**
	 * @param string $youtube
	 */
	public function setYoutube($youtube) {
		$this->youtube = $youtube;
	}

	/**
	 * @return string
	 */
	public function getFacebook() {
		return $this->facebook;
	}

	/**
	 * @param string $facebook
	 */
	public function setFacebook($facebook) {
		$this->facebook = $facebook;
	}

	/**
	 * @return string
	 */
	public function getSoundcloud() {
		return $this->soundcloud;
	}

	/**
	 * @param string $soundcloud
	 */
	public function setSoundcloud($soundcloud) {
		$this->soundcloud = $soundcloud;
	}

	/**
	 * @return string
	 */
	public function getVine() {
		return $this->vine;
	}

	/**
	 * @param string $vine
	 */
	public function setVine($vine) {
		$this->vine = $vine;
	}

	/**
	 * @return string
	 */
	public function getInstagram() {
		return $this->instagram;
	}

	/**
	 * @param string $instagram
	 */
	public function setInstagram($instagram) {
		$this->instagram = $instagram;
	}

	/**
	 * @return string
	 */
	public function getPseudoMinecraft() {
		return $this->pseudoMinecraft;
	}

	/**
	 * @param string $pseudoMinecraft
	 */
	public function setPseudoMinecraft($pseudoMinecraft) {
		$this->pseudoMinecraft = $pseudoMinecraft;
	}

	/**
	 * @return string
	 */
	public function getNewsground() {
		return $this->newsground;
	}

	/**
	 * @param string $newsground
	 */
	public function setNewsground($newsground) {
		$this->newsground = $newsground;
	}
}

