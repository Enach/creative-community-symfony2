<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CritiqueParagraphe
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_paragraphes")
 * @ORM\HasLifecycleCallbacks
 */
class Paragraphe {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Critique", inversedBy="paragraphes")
	 */
	private $critique;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Article", inversedBy="paragraphes")
	 */
	private $article;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $img;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text")
	 */
	private $content;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string", length=255)
	 */
	private $titre;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set critique
	 *
	 * @param string $critique
	 *
	 * @return Paragraphe
	 */
	public function setCritique($critique) {
		$this->critique = $critique;

		return $this;
	}

	/**
	 * Get critique
	 *
	 * @return string
	 */
	public function getCritique() {
		return $this->critique;
	}

	/**
	 * Set article
	 *
	 * @param $article
	 *
	 * @return Paragraphe
	 * @internal param string $critique
	 *
	 */
	public function setArticle($article) {
		$this->article = $article;

		return $this;
	}

	/**
	 * Get article
	 *
	 * @return string
	 */
	public function getArticle() {
		return $this->critique;
	}

	/**
	 * Set img
	 *
	 * @param Image $img
	 *
	 * @return Paragraphe
	 */
	public function setImg($img) {
		$this->img = $img;

		return $this;
	}

	/**
	 * Get img
	 *
	 * @return Image
	 */
	public function getImg() {
		return $this->img;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 *
	 * @return Paragraphe
	 */
	public function setContent($content) {
		$this->content = $content;

		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Set titre
	 *
	 * @param string $titre
	 *
	 * @return Paragraphe
	 */
	public function setTitre($titre) {
		$this->titre = $titre;

		return $this;
	}

	/**
	 * Get titre
	 *
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}
}

