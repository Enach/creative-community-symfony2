<?php

namespace CreaCom\MachimaniaBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Film
 *
 * @ORM\Table(name="machimania_film")
 * @ORM\Entity(repositoryClass="CreaCom\MachimaniaBundle\Entity\FilmRepository")
 */
class Film {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string", length=255)
	 */
	private $titre;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Casting", cascade={"persist", "remove"}
	 *                                                                       ,fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $casting;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="banniere", type="string", length=255)
	 */
	private $banniere;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="affiche", type="string", length=255)
	 */
	private $affiche;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $image1;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $image2;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $image3;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $image4;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $image5;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $bandeAnnonce;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Avis", cascade={"persist", "remove"},
	 *                                                                     mappedBy="film")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $avis;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\AvisSpectateurs", cascade={"persist",
	 *                                                                                "remove"}, mappedBy="film")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $avisSpectateurs;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lienFilm", type="string", length=255)
	 */
	private $lienFilm;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="studio", type="string", length=255, nullable=true)
	 */
	private $studio;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="sortie", type="datetime", length=255)
	 */
	private $sortie;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="genre", type="string", length=255)
	 */
	private $genre;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Jeux", fetch="EXTRA_LAZY")
	 */
	private $jeux;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="duree", type="datetime")
	 */
	private $duree;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="langue", type="string", length=255)
	 */
	private $langue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="format", type="string", length=255)
	 */
	private $format;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="synopsis", type="text")
	 */
	private $synopsis;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="publied")
	 */
	private $publied = false;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(type="datetime", nullable = true)
	 */
	private $publishDate;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * @param mixed $titre
	 */
	public function setTitre($titre) {
		$this->titre = $titre;
	}

	/**
	 * @return mixed
	 */
	public function getBanniere() {
		return $this->banniere;
	}

	/**
	 * @param mixed $banniere
	 */
	public function setBanniere($banniere) {
		$this->banniere = $banniere;
	}

	/**
	 * @return mixed
	 */
	public function getAffiche() {
		return $this->affiche;
	}

	/**
	 * @param mixed $affiche
	 */
	public function setAffiche($affiche) {
		$this->affiche = $affiche;
	}

	/**
	 * Set image1
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImage1($image) {
		$this->image1 = $image;

		return $this;
	}

	/**
	 * Get image1
	 *
	 * @return Image
	 */
	public function getImage1() {
		return $this->image1;
	}

	/**
	 * Set image2
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImage2($image) {
		$this->image2 = $image;

		return $this;
	}

	/**
	 * Get image2
	 *
	 * @return Image
	 */
	public function getImage2() {
		return $this->image2;
	}

	/**
	 * Set image3
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImage3($image) {
		$this->image3 = $image;

		return $this;
	}

	/**
	 * Get image3
	 *
	 * @return Image
	 */
	public function getImage3() {
		return $this->image3;
	}

	/**
	 * Set image2
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImage4($image) {
		$this->image4 = $image;

		return $this;
	}

	/**
	 * Get image4
	 *
	 * @return Image
	 */
	public function getImage4() {
		return $this->image4;
	}

	/**
	 * Set image5
	 *
	 * @param Image $image
	 *
	 * @return Image
	 */
	public function setImage5($image) {
		$this->image5 = $image;

		return $this;
	}

	/**
	 * Get image5
	 *
	 * @return Image
	 */
	public function getImage5() {
		return $this->image5;
	}

	/**
	 * @return mixed
	 */
	public function getLienFilm() {
		return $this->lienFilm;
	}

	/**
	 * @param mixed $lienFilm
	 */
	public function setLienFilm($lienFilm) {
		$this->lienFilm = $lienFilm;
	}

	/**
	 * @return mixed
	 */
	public function getStudio() {
		return $this->studio;
	}

	/**
	 * @param mixed $studio
	 */
	public function setStudio($studio) {
		$this->studio = $studio;
	}

	/**
	 * @return datetime
	 */
	public function getSortie() {
		return $this->sortie;
	}

	/**
	 * @param datetime $sortie
	 */
	public function setSortie($sortie) {
		$this->sortie = $sortie;
	}

	/**
	 * @return mixed
	 */
	public function getGenre() {
		return $this->genre;
	}

	/**
	 * @param mixed $genre
	 */
	public function setGenre($genre) {
		$this->genre = $genre;
	}

	/**
	 * @return Jeux
	 */
	public function getJeux() {
		return $this->jeux;
	}

	/**
	 * @param Jeux $jeux
	 */
	public function setJeux($jeux) {
		$this->jeux = $jeux;
	}

	/**
	 * @return string
	 */
	public function getDuree() {
		return $this->duree;
	}

	/**
	 * @param string $duree
	 */
	public function setDuree($duree) {
		$this->duree = $duree;
	}

	/**
	 * @return mixed
	 */
	public function getFormat() {
		return $this->format;
	}

	/**
	 * @param mixed $format
	 */
	public function setFormat($format) {
		$this->format = $format;
	}

	/**
	 * @return mixed
	 */
	public function getSynopsis() {
		return $this->synopsis;
	}

	/**
	 * @param mixed $synopsis
	 */
	public function setSynopsis($synopsis) {
		$this->synopsis = $synopsis;
	}

	/**
	 * Add Avis
	 *
	 * @param Avis $avis
	 *
	 * @return Avis
	 */
	public function addAvis(Avis $avis) {
		$this->avis[] = $avis;

		return $this;
	}

	/**
	 * Remove Avis
	 *
	 * @param Avis $avis
	 */
	public function removeAvis(Avis $avis) {
		$this->avis->removeElement($avis);
	}

	/**
	 * Get Avis
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAvis() {
		return $this->avis;
	}

	/**
	 * Add Avis Spectateurs
	 *
	 * @param AvisSpectateurs $avis
	 *
	 * @return Avis
	 */
	public function addAvisSpectateurs(AvisSpectateurs $avis) {
		$this->avisSpectateurs[] = $avis;

		return $this;
	}

	/**
	 * Remove Avis Spectateurs
	 *
	 * @param AvisSpectateurs $avis
	 */
	public function removeAvisSpectateurs(AvisSpectateurs $avis) {
		$this->avisSpectateurs->removeElement($avis);
	}

	/**
	 * Get Avis Spectateurs
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAvisSpectateurs() {
		return $this->avisSpectateurs;
	}

	/**
	 * @return Casting
	 */
	public function getCasting() {
		return $this->casting;
	}

	/**
	 * @param Casting $casting
	 */
	public function setCasting(Casting $casting) {
		$this->casting = $casting;
	}

	/**
	 * @return boolean
	 */
	public function isPublied() {
		return $this->publied;
	}

	/**
	 * @param boolean $publied
	 */
	public function setPublied($publied) {
		if ($publied) {
			$this->setPublishDate(new DateTime());
		} else {
			$this->setPublishDate(null);
		}
		$this->publied = $publied;
	}

	/**
	 * @return datetime
	 */
	public function getPublishDate() {
		return $this->publishDate;
	}

	/**
	 * @param datetime $publishDate
	 */
	public function setPublishDate($publishDate) {
		$this->publishDate = $publishDate;
	}

	/**
	 * @return string
	 */
	public function getLangue() {
		return $this->langue;
	}

	/**
	 * @param string $langue
	 */
	public function setLangue($langue) {
		$this->langue = $langue;
	}

	/**
	 * @return string
	 */
	public function getBandeAnnonce() {
		return $this->bandeAnnonce;
	}

	/**
	 * @param string $bandeAnnonce
	 */
	public function setBandeAnnonce($bandeAnnonce) {
		$this->bandeAnnonce = $bandeAnnonce;
	}
}