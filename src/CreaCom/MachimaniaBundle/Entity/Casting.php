<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Casting
 *
 * @ORM\Table(name="machimania_casting")
 * @ORM\Entity(repositoryClass="CreaCom\MachimaniaBundle\Entity\CastingRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Casting {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Film", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $film;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Serie", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $serie;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $realisation;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $ecriture;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $acteurPrimary;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $acteurSecondary;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $musique;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $tech;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $production;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $parts;

	/**
	 * @ORM\OneToOne(targetEntity="CreaCom\MachimaniaBundle\Entity\Image", cascade={"persist", "remove"})
	 */
	private $background;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->realisation     = new ArrayCollection();
		$this->ecriture        = new ArrayCollection();
		$this->acteurPrimary   = new ArrayCollection();
		$this->acteurSecondary = new ArrayCollection();
		$this->parts           = new ArrayCollection();
		$this->tech            = new ArrayCollection();
		$this->production      = new ArrayCollection();
		$this->musique         = new ArrayCollection();
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return Film
	 */
	public function getFilm() {
		return $this->film;
	}

	/**
	 * @param Film $film
	 */
	public function setFilm($film) {
		$this->film = $film;
	}

	/**
	 * Add realisateur
	 *
	 * @param array $realisateurs
	 *
	 * @return Casting
	 */
	public function addRealisation($realisateurs) {
		$this->realisation[] = $realisateurs;

		return $this;
	}

	/**
	 * Remove realisateur
	 *
	 * @param array $realisateurs
	 */
	public function removeRealisation($realisateurs) {
		$this->realisation->removeElement($realisateurs);
	}

	/**
	 * Get réalisateur
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getRealisation() {
		return $this->realisation;
	}

	/**
	 * Add ecriture
	 *
	 * @param array $ecriture
	 *
	 * @return Casting
	 */
	public function addEcriture($ecriture) {
		$this->ecriture[] = $ecriture;

		return $this;
	}

	/**
	 * Remove ecriture
	 *
	 * @param array $ecriture
	 */
	public function removeEcriture($ecriture) {
		$this->ecriture->removeElement($ecriture);
	}

	/**
	 * Get ecriture
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getEcriture() {
		return $this->ecriture;
	}

	/**
	 * Add ActeurPrimary
	 *
	 * @param array $ActeurPrimary
	 *
	 * @return Casting
	 */
	public function addActeurPrimary($ActeurPrimary) {
		$this->acteurPrimary[] = $ActeurPrimary;

		return $this;
	}

	/**
	 * Remove ActeurPrimary
	 *
	 * @param array $ActeurPrimary
	 */
	public function removeActeurPrimary($ActeurPrimary) {
		$this->acteurPrimary->removeElement($ActeurPrimary);
	}

	/**
	 * Get ActeurPrimary
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getActeurPrimary() {
		return $this->acteurPrimary;
	}

	/**
	 * Add acteurSecondary
	 *
	 * @param array $acteurSecondary
	 *
	 * @return Casting
	 */
	public function addActeurSecondary($acteurSecondary) {
		$this->acteurSecondary[] = $acteurSecondary;

		return $this;
	}

	/**
	 * Remove acteurSecondary
	 *
	 * @param array $acteurSecondary
	 */
	public function removeActeurSecondary($acteurSecondary) {
		$this->acteurSecondary->removeElement($acteurSecondary);
	}

	/**
	 * Get acteurSecondary
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getActeurSecondary() {
		return $this->acteurSecondary;
	}

	/**
	 * Add musique
	 *
	 * @param array $musique
	 *
	 * @return Casting
	 */
	public function addMusique($musique) {
		$this->musique[] = $musique;

		return $this;
	}

	/**
	 * Remove musique
	 *
	 * @param array $musique
	 */
	public function removeMusique($musique) {
		$this->musique->removeElement($musique);
	}

	/**
	 * Get musique
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getMusique() {
		return $this->musique;
	}

	/**
	 * Add techniques
	 *
	 * @param array $techniques
	 *
	 * @return Casting
	 */
	public function addTech($techniques) {
		$this->tech[] = $techniques;

		return $this;
	}

	/**
	 * Remove techniques
	 *
	 * @param array $techniques
	 */
	public function removeTech($techniques) {
		$this->tech->removeElement($techniques);
	}

	/**
	 * Get techniques
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getTech() {
		return $this->tech;
	}

	/**
	 * Add production
	 *
	 * @param array $production
	 *
	 * @return Casting
	 */
	public function addProduction($production) {
		$this->production[] = $production;

		return $this;
	}

	/**
	 * Remove production
	 *
	 * @param array $production
	 */
	public function removeProduction($production) {
		$this->production->removeElement($production);
	}

	/**
	 * Get production
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProduction() {
		return $this->production;
	}

	/**
	 * Add partenaires
	 *
	 * @param array $partenaire
	 *
	 * @return Casting
	 */
	public function addPart($partenaire) {
		$this->parts[] = $partenaire;

		return $this;
	}

	/**
	 * Remove partenaires
	 *
	 * @param array $partenaire
	 */
	public function removePart($partenaire) {
		$this->parts->removeElement($partenaire);
	}

	/**
	 * Get partenaires
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getParts() {
		return $this->parts;
	}

	/**
	 * @return Serie
	 */
	public function getSerie() {
		return $this->serie;
	}

	/**
	 * @param Serie $serie
	 */
	public function setSerie($serie) {
		$this->serie = $serie;
	}

	/**
	 * @return mixed
	 */
	public function getBackground() {
		return $this->background;
	}

	/**
	 * @param mixed $background
	 */
	public function setBackground($background) {
		$this->background = $background;
	}

}