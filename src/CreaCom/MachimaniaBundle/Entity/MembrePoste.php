<?php

namespace CreaCom\MachimaniaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poste
 *
 * @ORM\Entity()
 * @ORM\Table(name="machimania_poste")
 */
class MembrePoste {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToMany(targetEntity="CreaCom\MachimaniaBundle\Entity\Membre", fetch="EXTRA_LAZY", mappedBy="poste")
	 * @ORM\Column(nullable=true)
	 */
	private $membre;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	private $titre;
	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $intro;
	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	private $description;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean")
	 */
	private $etat;


	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get Membre
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getMembre() {
		return $this->membre;
	}

	/**
	 * Add membre
	 *
	 * @param $membre
	 *
	 * @return MembrePoste
	 */
	public function addMembre(Membre $membre) {
		$this->membre[] = $membre;

		return $this;
	}

	/**
	 * Remove membre
	 *
	 * @param $membre
	 */
	public function removeMembre(Membre $membre) {
		$this->membre->removeElement($membre);
	}

	/**
	 * @return string
	 */
	public function getTitre() {
		return $this->titre;
	}

	/**
	 * @param string $titre
	 */
	public function setTitre($titre) {
		$this->titre = $titre;
	}

	/**
	 * @return string
	 */
	public function getIntro() {
		return $this->intro;
	}

	/**
	 * @param string $intro
	 */
	public function setIntro($intro) {
		$this->intro = $intro;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return boolean
	 */
	public function isEtat() {
		return $this->etat;
	}

	/**
	 * @param boolean $etat
	 */
	public function setEtat($etat) {
		$this->etat = $etat;
	}
}

