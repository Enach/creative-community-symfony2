<?php

namespace CreaCom\MachimaniaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RechercheController extends Controller {

	public function articlesAction(Request $request, $type, $tri) {

		if ($type == "all") {

			$article = $this->getDoctrine()
				->getRepository('CreaComMachimaniaBundle:Article');

			$query = $article->createQueryBuilder('a')
				->where('a.publied = true')
				->orderBy('a.id', ':tri')
				->setMaxResults('3')
				->getQuery();

			$query->setParameter('tri', $tri);
		} else {

			$article = $this->getDoctrine()
				->getRepository('CreaComMachimaniaBundle:Article');

			$query = $article->createQueryBuilder('a')
				->where('a.publied = true')
				->orderBy('a.id', ':tri')
				->setMaxResults('3')
				->getQuery();

			$query->setParameter('tri', $tri);
		}

		$paginator = $this->get('knp_paginator');
		$articles  = $paginator->paginate(
			$query,
			$request->query->getInt('page', 1)/*page number*/,
			5/*limit per page*/
		);


		return $this->render('CreaComMachimaniaBundle:Recherches:articles.html.twig', array(
			'articles' => $articles,
		));
	}

	public function critiquesAction(Request $request, $type, $tri) {

		if ($type == "all") {

			$critique = $this->getDoctrine()
				->getRepository('CreaComMachimaniaBundle:Critique');

			$query = $critique->createQueryBuilder('a')
				->where('a.publied = true')
				->orderBy('a.id', ':tri')
				->setMaxResults('3')
				->getQuery();

			$query->setParameter('tri', $tri);
		} else {

			$critique = $this->getDoctrine()
				->getRepository('CreaComMachimaniaBundle:Critique');

			$query = $critique->createQueryBuilder('a')
				->where('a.publied' == true)
				->orderBy('a.id', ':tri')
				->setMaxResults('3')
				->getQuery();

			$query->setParameter('tri', $tri);
		}

		$paginator = $this->get('knp_paginator');
		$critique  = $paginator->paginate(
			$query,
			$request->query->getInt('page', 1)/*page number*/,
			5/*limit per page*/
		);

		return $this->render('CreaComMachimaniaBundle:Recherches:critiques.html.twig', array(
			'critiques' => $critique,
		));
	}

	public function seriesAction($jeu) {
		$conseillons = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Critique')->getConseillons("saison");

		return $this->render('CreaComMachimaniaBundle:Recherches:series.html.twig', array(
			'conseillons' => $conseillons,
			'jeu'         => $jeu,
		));
	}

	public function filmsAction($jeu) {
		$conseillons = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Critique')->getConseillons("saison");

		return $this->render('CreaComMachimaniaBundle:Recherches:film.html.twig', array(
			'conseillons' => $conseillons,
			'jeu'         => $jeu,
		));
	}

	public function machinimaAjaxAction(Request $request, $genre, $jeu, $tri, $langue, $format, $statut, $type) {
		if ($type == "serie") {
			$repository = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Serie');
		} else {
			$repository = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Film');
		}

		$query = $repository->createQueryBuilder('s');
		$query->where($query->expr()->like('s.genre', ':genre'))
			->where('s.genre LIKE :genre')
			->andWhere('s.jeux LIKE :jeu')
			->andWhere('s.langue LIKE :langue')
			->andWhere('s.format LIKE :format')
			->andWhere('s.etat LIKE :statut')
			->orderBy('a.id', ':tri')
			->setMaxResults('3')
			->getQuery();

		$query->setParameters(array(
			'genre'  => "%" . $genre . "%",
			'jeu'    => "%" . $jeu . "%",
			'langue' => "%" . $langue . "%",
			'format' => "%" . $format . "%",
			'satut'  => "%" . $statut . "%",
			'tri'    => "%" . $tri . "%",
		));

		$paginator = $this->get('knp_paginator');
		$series    = $paginator->paginate(
			$query,
			$request->query->getInt('page', 1)/*page number*/,
			5/*limit per page*/
		);

		return $this->render('CreaComMachimaniaBundle:Recherches:ajax.html.twig', array(
			'$series' => $series,
		));
	}

}