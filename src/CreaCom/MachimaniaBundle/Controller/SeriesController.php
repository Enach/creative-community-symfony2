<?php

namespace CreaCom\MachimaniaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SeriesController extends Controller {

	public function serieAction($serieId) {
		$serie = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Serie')->find($serieId);

		$articles = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Article')->getArticles($serie->getTitre());

		return $this->render('CreaComMachimaniaBundle:Serie:index.html.twig', array(
			'articles' => $articles,
		));
	}

	public function critiqueAction($serieId) {
		$serie = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Film')->find($serieId);

		$critique = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Critique')->findOneBy(array('serie' => $serie));

		return $this->render('CreaComMachimaniaBundle:Serie:critique.html.twig', array(
			'critique' => $critique,
			'serie'    => $serie,
		));
	}

	public function saisonAction($saisonId) {
		$saison = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Saison')->find($saisonId);

		return $this->render('CreaComMachimaniaBundle:Serie:saison.html.twig', array(
			'saison' => $saison,
		));

	}

	public function AvisSpectateur($serieId) {
		$serie = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Serie')->find($serieId);

		$avisSpectateurs = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:AvisSpectateur')->findBy(array('serie' => $serie));

		return $this->render('CreaComMachimaniaBundle:Serie:avisSpectateur.html.twig', array(
			'avisSpectateur' => $avisSpectateurs,
		));
	}
}
