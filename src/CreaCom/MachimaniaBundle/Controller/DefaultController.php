<?php

namespace CreaCom\MachimaniaBundle\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CreaCom\MachimaniaBundle\Entity\MachinimaDay;

class DefaultController extends Controller {
	public function indexAction() {
		$tempDate = new DateTime();
		$tempDate = $tempDate->format('Y-m-d 00:00:00');
		$today    = new DateTime($tempDate);

		$article = $this->getDoctrine()
			->getRepository('CreaComMachimaniaBundle:Article');

		$query = $article->createQueryBuilder('a')
			->orderBy('a.id', 'ASC')
			->setMaxResults('3')
			->getQuery();

		$articles = $query->getArrayResult();

		$critique = $this->getDoctrine()
			->getRepository('CreaComMachimaniaBundle:Critique');

		$query = $critique->createQueryBuilder('c')
			->orderBy('c.id', 'ASC')
			->setMaxResults('3')
			->getQuery();

		$critiques = $query->getArrayResult();

		$film = $this->getDoctrine()
			->getRepository('CreaComMachimaniaBundle:Film');

		$query = $film->createQueryBuilder('f')
			->where('f.sortie < :today')
			->orderBy('f.id', 'ASC')
			->setMaxResults('3')
			->getQuery();

		$query->setParameter('today', $today);

		$films = $query->getArrayResult();

		$serie = $this->getDoctrine()
			->getRepository('CreaComMachimaniaBundle:Serie');

		$query = $serie->createQueryBuilder('s')
			->where('s.sortie < :today')
			->orderBy('s.id', 'ASC')
			->setMaxResults('3')
			->getQuery();

		$query->setParameter('today', $today);

		$series = $query->getArrayResult();

		$fiches = $films + $series;

		uasort($fiches, function ($a, $b) {
			global $fiches;

			return strcmp($fiches[$a]['publishDate'], $fiches[$b]['publishDate']);
		});

		$bestCritique = $this->getDoctrine()
			->getRepository('CreaComMachimaniaBundle:Critique');

		$query = $bestCritique->createQueryBuilder('c')
			->orderBy('c.note', 'DESC')
			->setMaxResults('4')
			->getQuery();

		$bestCritiques = $query->getArrayResult();

		$machinimaDay = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:MachinimaDay')->findOneBy(
			array(
				'day' => $today,
			)
		);

		$query = $film->createQueryBuilder('f')
			->where('f.sortie > :today')
			->orderBy('f.id', 'ASC')
			->setMaxResults('3')
			->getQuery();

		$query->setParameter('today', $today);

		$filmSoon = $query->getArrayResult();

		$query = $serie->createQueryBuilder('s')
			->where('s.sortie > :today')
			->orderBy('s.id', 'ASC')
			->setMaxResults('3')
			->getQuery();

		$query->setParameter('today', $today);

		$serieSoon = $query->getArrayResult();

		$releaseSoon = $filmSoon + $serieSoon;

		uasort($releaseSoon, function ($a, $b) {
			global $releaseSoon;

			return strcmp($releaseSoon[$a]['publishDate'], $releaseSoon[$b]['publishDate']);
		});

		return $this->render('CreaComMachimaniaBundle:Default:index.html.twig', array(
			'articles'      => $articles,
			'critiques'     => $critiques,
			'fiches'        => $fiches,
			'bestCritiques' => $bestCritiques,
			'machinimaDay'  => $machinimaDay,
			'releaseSoon'   => $releaseSoon,
		));
	}

	public function partenairesAction() {
		$partnaires = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Partenaires')->findAll();

		return $this->render('CreaComMachimaniaBundle:Default:partenaires.html.twig', array(
			'partenaires' => $partnaires,
		));
	}

	public function recrutementAction() {
		$membres = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Membre')->findAll();
		$postes  = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:MembrePoste')->findBy(array(
			'etat' => 1,
		));

		return $this->render('CreaComMachimaniaBundle:Default:recrutement.html.twig', array(
			'membres' => $membres,
			'postes'  => $postes,
		));
	}

	public function profilMembreAction($id) {
		$membre = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Membre')->find($id);

		return $this->render('CreaComMachimaniaBundle:Default:profilMembre.html.twig', array(
			'membre' => $membre,
		));
	}

	public function mentionsLegalesAction() {
		return $this->render('CreaComMachimaniaBundle:Default:mentionsLegales.html.twig');
	}

	public function cguAction() {
		return $this->render('CreaComMachimaniaBundle:Default:cgu.html.twig');
	}
}
