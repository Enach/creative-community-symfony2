<?php

namespace CreaCom\MachimaniaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FilmsController extends Controller {

	public function filmAction($filmId) {
		$film = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Film')->find($filmId);

		$articles = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Article')->getArticles($film->getTitre());

		return $this->render('CreaComMachimaniaBundle:Film:index.html.twig', array(
			'articles' => $articles,
		));
	}

	public function critiqueAction($filmId) {
		$film = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Film')->find($filmId);

		$critique = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Critique')->findOneBy(array('film' => $film));

		return $this->render('CreaComMachimaniaBundle:Film:critique.html.twig', array(
			'critique' => $critique,
			'film'     => $film,
		));
	}

	public function avisSpectateurAction($filmId) {
		$film = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Film')->find($filmId);

		$avisSpectateurs = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:AvisSpectateur')->findBy(array('film' => $film));

		return $this->render('CreaComMachimaniaBundle:Film:avisSpectateur.html.twig', array(
			'avisSpectateur' => $avisSpectateurs,
		));
	}
}
