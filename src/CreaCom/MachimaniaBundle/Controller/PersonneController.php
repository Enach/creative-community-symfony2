<?php

namespace CreaCom\MachimaniaBundle\Controller;

use CreaCom\MachimaniaBundle\Entity\Film;
use CreaCom\MachimaniaBundle\Entity\Serie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PersonneController extends Controller {

	public function personneAction($personneId) {
		$personne = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Personne')->find($personneId);

		$participations = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Personne')->getParticipations($personne);

		/** @var array $articles1 */
		$articles1 = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Article')->getArticles($personne->getNom());
		/** @var array $articles2 */
		$articles2 = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Article')->getArticles($personne->getPseudo());

		$articles = array();
		array_push($articles, $articles1);
		array_push($articles, $articles2);

		function cmp($a, $b) {
			global $articles;

			return strcmp($articles[$a]['date'], $articles[$b]['date']);
		}

		uksort($articles, 'cmp');

		$note           = $this->getDoctrine()->getRepository('CreaComMachimaniaBundle:Critique')->getNoteGlobale($participations['films'], $participations['series']);
		$doneProduction = array();

		/** @var Film $film */
		foreach ($participations['films'] as $film) {
			if ($film->getSortie() <= new \DateTime()) {
				$doneProduction++;
			}
		}
		/** @var Serie $serie */
		foreach ($participations['series'] as $serie) {
			if ($film->getSortie() <= new \DateTime()) {
				$doneProduction++;
			}
		}

		return $this->render('CreaComMachimaniaBundle:Personne:index.html.twig', array(
			'personne'       => $personne,
			'participations' => $participations,
			'articles'       => $articles,
			'doneProduction' => $doneProduction,
			'note'           => $note,
		));

	}

}