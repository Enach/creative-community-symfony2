<?php

namespace CreaCom\AGBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use CreaCom\AGBundle\Entity\Candidat;
use CreaCom\AGBundle\Entity\Voting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VoteController extends Controller {
	public function creationVoteAction($annee) {
		$em     = $this->get('doctrine.orm.default_entity_manager');
		$users  = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findAll();
		$postes = $this->getDoctrine()->getRepository('CreaComAGBundle:Poste')->findBy(array('annee' => $annee));

		/** @var User $user */
		foreach ($users as $user) {
			$checkProcuration = $this->getDoctrine()->getRepository('CreaComAGBundle:Procuration')->findBy(array('membre' => $user, 'annee' => $annee));
			if (0 == count($checkProcuration)) {
				foreach ($postes as $poste) {
					$existVote = $this->getDoctrine()->getRepository('CreaComAGBundle:Voting')->findOneBy(array('membre' => $user, 'annee' => $annee, 'poste' => $poste));
					if (null === $existVote) {
						$vote      = new Voting();
						$rolesUser = $user->getGroups();
						if (in_array($poste->getGroupe(), $rolesUser->toArray()) or null == $poste->getGroupe()) {
							$power = 1;
						} else {
							$power = 0;
						}
						$procurations = $this->getDoctrine()->getRepository('CreaComAGBundle:Procuration')->findBy(array('toMembre' => $user, 'annee' => $annee));
						if (0 != count($procurations)) {
							$procuration      = $this->getDoctrine()->getRepository('CreaComAGBundle:Procuration')->findOneBy(array('toMembre' => $user, 'annee' => $annee));
							$rolesProcuration = $procuration->getMembre()->getGroups();
							if (in_array($poste->getGroupe(), $rolesProcuration->toArray())) {
								for ($i = 1; $i < count($procurations) + 1; $i++) {
									$power++;
								}
							}
						}
						$vote->setPower($power);
						$vote->setAnnee($annee);
						$vote->setMembre($user);
						$vote->setPoste($poste);
						$em->persist($vote);
					}
				}
			}
		}
		$em->flush();

		return $this->redirectToRoute('crea_com_ag_result', array('annee' => $annee));
	}

	public function startVotingAction($annee, $poste) {
		$em    = $this->get('doctrine.orm.default_entity_manager');
		$votes = $this->getDoctrine()->getRepository('CreaComAGBundle:Voting')->findBy(array('annee' => $annee, 'poste' => $poste));

		foreach ($votes as $vote) {
			$vote->setOpen(1);
			$em->persist($vote);
		}
		$em->flush();

		return $this->redirectToRoute('crea_com_ag_result', array('annee' => $annee));
	}

	public function closeVotingAction($annee, $poste) {
		$em    = $this->get('doctrine.orm.default_entity_manager');
		$votes = $this->getDoctrine()->getRepository('CreaComAGBundle:Voting')->findBy(array('annee' => $annee, 'poste' => $poste));

		foreach ($votes as $vote) {
			$vote->setOpen(0);
			$em->persist($vote);
		}
		$em->flush();

		return $this->redirectToRoute('crea_com_ag_result', array('annee' => $annee));
	}

	public function resultVotingAction($annee) {
		$postes    = $this->getDoctrine()->getRepository('CreaComAGBundle:Poste')->findBy(array('annee' => $annee));
		$candidats = $this->getDoctrine()->getRepository('CreaComAGBundle:Candidat')->findBy(array('annee' => $annee));
		$maxVote   = array();
		$votesOpen = array();
		foreach ($postes as $poste) {
			$max   = 0;
			$votes = $this->getDoctrine()->getRepository('CreaComAGBundle:Voting')->findBy(array('annee' => $annee, 'poste' => $poste));
			foreach ($votes as $vote) {
				$max += $vote->getPower();
			}
			$maxVote[$poste->getId()]   = $max;
			$votesOpened                = $this->getDoctrine()->getRepository('CreaComAGBundle:Voting')->findBy(array('annee' => $annee, 'poste' => $poste, 'open' => 1));
			$votesOpen[$poste->getId()] = (count($votesOpened) > 0);
		}

		return $this->render('CreaComAGBundle:Vote:resultat.html.twig', array(
			'candidats'  => $candidats,
			'postes'     => $postes,
			'annee'      => $annee,
			'maxVote'    => $maxVote,
			'admin_pool' => $this->get('sonata.admin.pool'),
			'votesOpen'  => $votesOpen,
		));
	}

	public function currentVoteAction(Request $request, $annee) {
		$membre    = $this->getUser();
		$em        = $this->get('doctrine.orm.default_entity_manager');
		$candidats = $this->getDoctrine()->getRepository('CreaComAGBundle:Candidat')->findBy(array('annee' => $annee));
		$vote      = $this->getDoctrine()->getRepository('CreaComAGBundle:Voting')->findOneBy(array('membre' => $membre, 'open' => 1, 'annee' => $annee));
		if (null !== $vote) {
			$power = $vote->getPower();
			if (null != $power) {
				$candidats = $this->getDoctrine()->getRepository('CreaComAGBundle:Candidat')->findBy(array('poste' => $vote->getPoste(), 'annee' => $vote->getAnnee()));
				$power     = $vote->getPower();

				$candidatsListe = array();

				foreach ($candidats as $candidat) {
					$candidat = array('' . $candidat->getId() . '' => $candidat->getMembre()->getUsername());
					array_push($candidatsListe, $candidat);
				}
				array_push($candidatsListe, array(null => 'Vote Blanc'));

				$votingForm = $this->createFormBuilder()->add('vote', 'submit', array('label' => 'Voter'));

				for ($i = 1; $i < $power + 1; $i++) {
					$votingForm->add('voix' . $i . '', 'choice', array(
						'expanded' => true,
						'multiple' => false,
						'label'    => "Vote " . $i,
						'choices'  => $candidatsListe,
					));
				}
				$numberChoice = $i;
				$votingForm   = $votingForm->getForm();

				$votingForm->handleRequest($request);
				if ($votingForm->isValid()) {
					for ($i = 1; $i < $power + 1; $i++) {
						/** @var Candidat $candidat */
						$candidat = $votingForm->get('voix' . $i . '')->getData();
						if (null !== $candidat) {
							$candidat = $this->getDoctrine()->getRepository('CreaComAGBundle:Candidat')->find($candidat);

							$actualSolde = $candidat->getSolde();
							$candidat->setSolde($actualSolde + 1);
							$em->persist($candidat);
						}
						$actualPower = $vote->getPower();
						$vote->setPower($actualPower - 1);
						$em->persist($vote);
					}
					$em->flush();
					$this->get('session')->set('vote', $vote->getPoste()->getNom());

					return $this->redirectToRoute('crea_com_ag_homepage', array('annee' => $annee));
				}

				return $this->render('CreaComAGBundle:Vote:index.html.twig', array(
					'form'         => $votingForm->createView(),
					'candidats'    => $candidats,
					'admin_pool'   => $this->get('sonata.admin.pool'),
					'numberChoice' => $numberChoice,
				));
			}

			return $this->render('CreaComAGBundle:Vote:index.html.twig', array(
				'candidats'  => $candidats,
				'admin_pool' => $this->get('sonata.admin.pool'),
			));
		}

		return $this->render('CreaComAGBundle:Vote:index.html.twig', array(
			'candidats'  => $candidats,
			'admin_pool' => $this->get('sonata.admin.pool'),
		));
	}
}