<?php

namespace CreaCom\AGBundle\Entity;


use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Procuration
 *
 * @ORM\Table(name="ag_procuration")
 * @ORM\Entity(repositoryClass="CreaCom\AGBundle\Entity\ProcurationRepository")
 */
class Procuration {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 */
	private $membre;

	/**
	 * @ORM\OneToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 */
	private $toMembre;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="annee", type="string", length=255)
	 */
	private $annee;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get membre
	 *
	 * @return User
	 */
	public function getMembre() {
		return $this->membre;
	}

	/**
	 * Set membre
	 *
	 * @param User $membre
	 *
	 * @return Procuration
	 */
	public function setMembre(User $membre) {
		$this->membre = $membre;

		return $this;
	}

	/**
	 * Get toMembre
	 *
	 * @return User
	 */
	public function getToMembre() {
		return $this->toMembre;
	}

	/**
	 * Set toMembre
	 *
	 * @param User $toMembre
	 *
	 * @return Procuration
	 */
	public function setToMembre(User $toMembre) {
		$this->toMembre = $toMembre;

		return $this;
	}

	/**
	 * Get annee
	 *
	 * @return string
	 */
	public function getAnnee() {
		return $this->annee;
	}

	/**
	 * Set annee
	 *
	 * @param string $annee
	 *
	 * @return Procuration
	 */
	public function setAnnee($annee) {
		$this->annee = $annee;

		return $this;
	}
}

