<?php

namespace CreaCom\AGBundle\Entity;


use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Candidat
 *
 * @ORM\Table(name="ag_candidats")
 * @ORM\Entity(repositoryClass="CreaCom\AGBundle\Entity\CandidatRepository")
 */
class Candidat {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 */
	private $membre;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\AGBundle\Entity\Poste", fetch="EXTRA_LAZY")
	 */
	private $poste;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="profession", type="text")
	 */
	private $profession;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="solde", type="integer")
	 */
	private $solde = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="annee", type="string", length=255)
	 */
	private $annee;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get membre
	 *
	 * @return User
	 */
	public function getMembre() {
		return $this->membre;
	}

	/**
	 * Set Membre
	 *
	 * @param User $membre
	 *
	 * @return Candidat
	 */
	public function setMembre(User $membre) {
		$this->membre = $membre;

		return $this;
	}

	/**
	 * Get poste
	 *
	 * @return Poste
	 */
	public function getPoste() {
		return $this->poste;
	}

	/**
	 * Set Poste
	 *
	 * @param Poste $poste
	 *
	 * @return Candidat
	 */
	public function setPoste(Poste $poste) {
		$this->poste = $poste;

		return $this;
	}

	/**
	 * Get profession
	 *
	 * @return string
	 */
	public function getProfession() {
		return $this->profession;
	}

	/**
	 * Set profession
	 *
	 * @param string $profession
	 *
	 * @return Candidat
	 */
	public function setProfession($profession) {
		$this->profession = $profession;

		return $this;
	}

	/**
	 * Get solde
	 *
	 * @return integer
	 */
	public function getSolde() {
		return $this->solde;
	}

	/**
	 * Set solde
	 *
	 * @param integer $solde
	 *
	 * @return Candidat
	 */
	public function setSolde($solde) {
		$this->solde = $solde;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAnnee() {
		return $this->annee;
	}

	/**
	 * @param string $annee
	 */
	public function setAnnee($annee) {
		$this->annee = $annee;
	}
}

