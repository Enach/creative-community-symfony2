<?php

namespace CreaCom\AGBundle\Entity;

use Application\Sonata\UserBundle\Entity\Group;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User;

/**
 * Poste
 *
 * @ORM\Table(name="ag_postes")
 * @ORM\Entity(repositoryClass="CreaCom\AGBundle\Entity\PosteRepository")
 */
class Poste {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text")
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="requierement", type="text")
	 */
	private $requierement;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $actual;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="duree", type="string", length=255)
	 */
	private $duree;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="annee", type="string", length=255)
	 */
	private $annee;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\Group", fetch="EXTRA_LAZY")
	 */
	private $groupe;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set nom
	 *
	 * @param string $nom
	 *
	 * @return Poste
	 */
	public function setNom($nom) {
		$this->nom = $nom;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Poste
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get actual
	 *
	 * @return User
	 */
	public function getActual() {
		return $this->actual;
	}

	/**
	 * Set actual
	 *
	 * @param User $actual
	 *
	 * @return Poste
	 */
	public function setActual(User $actual) {
		$this->actual = $actual;

		return $this;
	}

	/**
	 * Get duree
	 *
	 * @return string
	 */
	public function getDuree() {
		return $this->duree;
	}

	/**
	 * Set duree
	 *
	 * @param string $duree
	 *
	 * @return Poste
	 */
	public function setDuree($duree) {
		$this->duree = $duree;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAnnee() {
		return $this->annee;
	}

	/**
	 * @param string $annee
	 */
	public function setAnnee($annee) {
		$this->annee = $annee;
	}

	/**
	 * @return string
	 */
	public function getRequierement() {
		return $this->requierement;
	}

	/**
	 * @param string $requierement
	 */
	public function setRequierement($requierement) {
		$this->requierement = $requierement;
	}

	/**
	 * @return Group
	 */
	public function getGroupe() {
		return $this->groupe;
	}

	/**
	 * @param Group $groupe
	 *
	 * @return Poste
	 */
	public function setGroupe($groupe) {
		$this->groupe = $groupe;
	}
}

