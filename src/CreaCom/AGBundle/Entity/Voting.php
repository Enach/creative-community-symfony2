<?php

namespace CreaCom\AGBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Voting
 *
 * @ORM\Table(name="ag_voting")
 * @ORM\Entity(repositoryClass="CreaCom\AGBundle\Entity\VotingRepository")
 */
class Voting {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreaCom\AGBundle\Entity\Poste", fetch="EXTRA_LAZY")
	 */
	private $poste;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="power", type="integer")
	 */
	private $power = 1;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 */
	private $membre;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="annee", type="string", length=255)
	 */
	private $annee;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="validate", type="boolean")
	 */
	private $validate = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="open", type="boolean")
	 */
	private $open = false;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get poste
	 *
	 * @return Poste
	 */
	public function getPoste() {
		return $this->poste;
	}

	/**
	 * Set poste
	 *
	 * @param Poste $poste
	 *
	 * @return Voting
	 */
	public function setPoste(Poste $poste) {
		$this->poste = $poste;

		return $this;
	}

	/**
	 * Get power
	 *
	 * @return integer
	 */
	public function getPower() {
		return $this->power;
	}

	/**
	 * Set power
	 *
	 * @param integer $power
	 *
	 * @return Voting
	 */
	public function setPower($power) {
		$this->power = $power;

		return $this;
	}

	/**
	 * Get membre
	 *
	 * @return User
	 */
	public function getMembre() {
		return $this->membre;
	}

	/**
	 * Set membre
	 *
	 * @param User $membre
	 *
	 * @return Voting
	 */
	public function setMembre(User $membre) {
		$this->membre = $membre;

		return $this;
	}

	/**
	 * Get annee
	 *
	 * @return string
	 */
	public function getAnnee() {
		return $this->annee;
	}

	/**
	 * Set annee
	 *
	 * @param string $annee
	 *
	 * @return Voting
	 */
	public function setAnnee($annee) {
		$this->annee = $annee;

		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isOpen() {
		return $this->open;
	}

	/**
	 * @param boolean $open
	 */
	public function setOpen($open) {
		$this->open = $open;
	}

	/**
	 * @return boolean
	 */
	public function isValidate() {
		return $this->validate;
	}

	/**
	 * @param boolean $validate
	 */
	public function setValidate($validate) {
		$this->validate = $validate;
	}
}

