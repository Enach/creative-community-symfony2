<?php

namespace CreaCom\AGBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\FormatterBundle\Formatter\Pool;
use Sonata\FormatterBundle\Formatter\Pool as FormatterPool;

class PosteAdmin extends Admin {
	/**
	 * @var Pool
	 */
	protected $formatterPool;

	// Fields to be shown on create/edit forms

	/**
	 * @param \Sonata\FormatterBundle\Formatter\Pool $formatterPool
	 */
	public function setPoolFormatter(FormatterPool $formatterPool) {
		$this->formatterPool = $formatterPool;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Le Poste', array(
				'class' => 'col-md-6',
			))
			->add('nom', 'text', array(
				'label' => 'Nom du poste',
			))
			->add('description', 'textarea', array(
				'label' => 'Description du poste',
				'attr'  => array('class' => 'tinymce'),
			))
			->add('requierement', 'textarea', array(
				'label' => 'Pré-requis au poste',
				'attr'  => array('class' => 'tinymce'),
			))
			->end()
			->with('Le contexte', array(
				'class' => 'col-md-6',
			))
			->add('actual', 'entity', array(
				'required' => true,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('groupe', 'entity', array(
				'required' => true,
				'class'    => 'ApplicationSonataUserBundle:Group',
				'property' => 'name',
			))
			->add('duree', 'text', array(
				'label' => 'duree',
			))
			->add('annee', 'text', array(
				'label' => 'Année',
			))
			->end();
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('nom')
			->add('groupe')
			->add('duree')
			->add('annee');
	}
}