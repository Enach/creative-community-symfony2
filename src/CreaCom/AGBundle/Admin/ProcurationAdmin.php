<?php

namespace CreaCom\AGBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\FormatterBundle\Formatter\Pool;
use Sonata\FormatterBundle\Formatter\Pool as FormatterPool;

class ProcurationAdmin extends Admin {
	/**
	 * @var Pool
	 */
	protected $formatterPool;

	// Fields to be shown on create/edit forms

	/**
	 * @param \Sonata\FormatterBundle\Formatter\Pool $formatterPool
	 */
	public function setPoolFormatter(FormatterPool $formatterPool) {
		$this->formatterPool = $formatterPool;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Nouvelle procuration', array(
				'class' => 'col-md-12',
			))
			->add('membre', 'entity', array(
				'required' => true,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('toMembre', 'entity', array(
				'required' => true,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('annee', 'text', array(
				'label' => 'Ann�e',
			))
			->end();
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('membre.username')
			->add('toMembre.username')
			->add('annee');
	}
}