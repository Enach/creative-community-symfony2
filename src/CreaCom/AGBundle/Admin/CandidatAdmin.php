<?php

namespace CreaCom\AGBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\FormatterBundle\Formatter\Pool as FormatterPool;
use Sonata\FormatterBundle\Formatter\Pool;

class CandidatAdmin extends Admin {
	/**
	 * @var Pool
	 */
	protected $formatterPool;

	// Fields to be shown on create/edit forms

	/**
	 * @param \Sonata\FormatterBundle\Formatter\Pool $formatterPool
	 */
	public function setPoolFormatter(FormatterPool $formatterPool) {
		$this->formatterPool = $formatterPool;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->with('Ajout d\'une candidature', array(
				'class' => 'col-md-12',
			))
			->add('membre', 'entity', array(
				'required' => true,
				'class'    => 'ApplicationSonataUserBundle:User',
				'property' => 'username',
			))
			->add('poste', 'entity', array(
				'required' => true,
				'class'    => 'CreaComAGBundle:Poste',
				'property' => 'nom',
			))
			->add('profession', 'text', array(
				'required' => false,
				'label'    => 'Profession de foi',
			))
			->add('annee', 'text', array(
				'label' => 'Année',
			))
			->end();
	}

	protected function configureListFields(ListMapper $listMapper) {
		$listMapper
			->add('membre')
			->add('poste.nom')
			->add('solde')
			->add('annee')
			->add('_action', 'actions', array(
				'actions' => array(
					'edit' => array(),
				),
			));
	}
}