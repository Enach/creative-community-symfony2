<?php

namespace CreaCom\TodoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use CreaCom\TodoBundle\Entity\Colonne;

/**
 * Tableau
 *
 * @ORM\Table(name="td_table")
 * @ORM\Entity()
 */
class Tableau {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="projet", type="string", length=255)
	 */
	private $projet;

	/**
	 * @var array
	 *
	 * @ORM\ManyToOne(targetEntity="CreaCom\TodoBundle\Entity\Colonne", fetch="EXTRA_LAZY")
	 */
	private $colonne;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="background", type="string", length=255)
	 */
	private $background;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getProjet() {
		return $this->projet;
	}

	/**
	 * @param string $projet
	 */
	public function setProjet($projet) {
		$this->projet = $projet;
	}

	/**
	 * @param Colonne $colonne
	 *
	 * @return Tableau
	 */
	public function addColonne(Colonne $colonne) {
		$this->colonne[] = $colonne;

		return $this;
	}

	/**
	 * @param Colonne $colonne
	 */
	public function removeColonne(Colonne $colonne) {
		$this->colonne->removeElement($colonne);
	}

	/**
	 * @return ArrayCollection
	 */
	public function geColonne() {
		return $this->colonne;
	}

	/**
	 * @return string
	 */
	public function getBackground() {
		return $this->background;
	}

	/**
	 * @param string $background
	 */
	public function setBackground($background) {
		$this->background = $background;
	}
}

