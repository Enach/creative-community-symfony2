<?php

namespace CreaCom\TodoBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use CreaCom\TodoBundle\Entity\Colonne;

/**
 * Task
 *
 * @ORM\Table(name="td_task")
 * @ORM\Entity(repositoryClass="CreaCom\TodoBundle\Entity\TaskRepository")
 */
class Task {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Tableau
	 *
	 * @ORM\ManyToOne(targetEntity="CreaCom\TodoBundle\Entity\Colonne", fetch="EXTRA_LAZY")
	 */
	private $colonne;

	/**
	 * @@var ArrayCollection
	 *
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", fetch="EXTRA_LAZY")
	 * @ORM\Column(nullable=true)
	 */
	private $membre;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="background", type="string", length=255)
	 */
	private $background;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="deadline", type="datetime")
	 */
	private $deadline;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="etiquette", type="text")
	 */
	private $etiquette;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="checkbox", type="text")
	 */
	private $checkbox;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="comment", type="text")
	 */
	private $comment;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="progression", type="integer")
	 */
	private $progression;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="archive", type="boolean")
	 */
	private $archive;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return Colonne
	 */
	public function getColonne() {
		return $this->colonne;
	}

	/**
	 * @param Colonne $colonne
	 */
	public function setColonne(Colonne $colonne) {
		$this->colonne = $colonne;
	}

	/**
	 * @return string
	 */
	public function getBackground() {
		return $this->background;
	}

	/**
	 * @param string $background
	 */
	public function setBackground($background) {
		$this->background = $background;
	}

	/**
	 * @return DateTime
	 */
	public function getDeadline() {
		return $this->deadline;
	}

	/**
	 * @param DateTime $deadline
	 */
	public function setDeadline($deadline) {
		$this->deadline = $deadline;
	}

	/**
	 * @param String $etiquette
	 *
	 * @return Tableau
	 */
	public function addEtiquette($etiquette) {
		$this->etiquette[] = $etiquette;

		return $this;
	}

	/**
	 * @param String $etiquette
	 */
	public function removeEtiquette($etiquette) {
		$this->etiquette->removeElement($etiquette);
	}

	/**
	 * @return ArrayCollection
	 */
	public function getEtiquette() {
		return $this->etiquette;
	}

	/**
	 * @param String $checkbox
	 *
	 * @return Tableau
	 */
	public function addCheckbox($checkbox) {
		$this->checkbox[] = $checkbox;

		return $this;
	}

	/**
	 * @param String $checkbox
	 */
	public function removeCheckbox($checkbox) {
		$this->checkbox->removeElement($checkbox);
	}

	/**
	 * @return ArrayCollection
	 */
	public function getCheckbox() {
		return $this->checkbox;
	}

	/**
	 * @return array
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * @param array $comment
	 */
	public function setComment($comment) {
		$this->comment = $comment;
	}

	/**
	 * @return int
	 */
	public function getProgression() {
		return $this->progression;
	}

	/**
	 * @param int $progression
	 */
	public function setProgression($progression) {
		$this->progression = $progression;
	}

	/**
	 * @param User $user
	 *
	 * @return Task
	 */
	public function addMembre(User $user) {
		$this->membre[] = $user;

		return $this;
	}

	/**
	 * @param User $user
	 */
	public function removeMembre(User $user) {
		$this->membre->removeElement($user);
	}

	/**
	 * @return ArrayCollection
	 */
	public function getMembres() {
		return $this->membre;
	}

	/**
	 * @return boolean
	 */
	public function isArchive() {
		return $this->archive;
	}

	/**
	 * @param boolean $archive
	 */
	public function setArchive($archive) {
		$this->archive = $archive;
	}
}