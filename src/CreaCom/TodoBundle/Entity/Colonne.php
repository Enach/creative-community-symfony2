<?php
/**
 * Created by PhpStorm.
 * User: jouzu
 * Date: 24/01/2016
 * Time: 19:40
 */

namespace CreaCom\TodoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colonne
 *
 * @ORM\Table(name="td_colonne")
 * @ORM\Entity()
 */
class Colonne {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}
}